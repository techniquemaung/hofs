package com.hofs.fengshuicalendar.models;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="result")
public class Result {
	@Element(name="code")
	public int code;
	
	@Element
	public String description;
	
	@Element(name="download")
	public Download download;
	@Element
	public Data data;
	
	 public Result() {
	      super();
	   }  

	 public int getCode() {
	      return code;
	   }
	 public String getDescription() {
	      return description;
	   }
	 public Download getDownload() {
	      return download;
	   }
	 public Data getData() {
	      return data;
	   }
	   
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Result\n\ncode:\n");
		builder.append(code);
		builder.append("\n\ndescription:\n");
		builder.append(description);
		builder.append("\n\ndownload:");
		builder.append(download);
		builder.append("\n\ndata:");
		builder.append(data);
		
		return builder.toString();
	}

	
	
}
