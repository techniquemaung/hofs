package com.hofs.fengshuicalendar.models;

import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

public class DayBreaker {
	@Attribute
	int id;
	
	@Element(data=true)
	String name_en;
	
	@Element(data=true)
	String name_cn;
	
	@Element(data=true)
	String three_harmony_1;
	
	@Element(data=true)
	String three_harmony_2;
	
	@Element(data=true)
	String three_harmony_3;
	
	@Element(data=true)
	String six_harmony_1;
	
	@Element(data=true)
	String six_harmony_2;
	
	@Element(data=true)
	String auspicious;
	
	@Element(data=true)
	String inauspicious_hour;
	
	 public int getId() {
	      return id;
	   }

	   public String getName_en() {
	      return name_en;
	   }
	   public String getName_cn() {
		      return name_cn;
		   }
	   public String getThree_harmony_1() {
		      return three_harmony_1;
		   }
	   public String getThree_harmony_2() {
		      return three_harmony_2;
		   }
	   public String getThree_harmony_3() {
		      return three_harmony_3;
		   }
	   public String getSix_harmony_1() {
		      return six_harmony_1;
		   }
	   public String getSix_harmony_2() {
		      return six_harmony_2;
		   }
	   public String getAuspicious() {
		      return auspicious;
		   }
	   public String getInauspicious_hour(){
		   return inauspicious_hour;
	   }
	@Override
  	public String toString() {
  		StringBuilder builder = new StringBuilder();
  		
  		
  		builder.append("\nid\n");
  		builder.append(id);
  		builder.append("\nName_En\n");
  		builder.append(name_en);
  		builder.append("\nName_Cn");
  		builder.append(name_cn);
  		
  		return builder.toString();
  	}
}
