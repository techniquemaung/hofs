package com.hofs.fengshuicalendar.models;

import org.simpleframework.xml.Element;
/*
 *   <hs_11pm_1259am>average</hs_11pm_1259am>
                    <hs_01am_0259am>bad</hs_01am_0259am>
                    <hs_03am_0459am>best</hs_03am_0459am>
                    <hs_05am_0659am>average</hs_05am_0659am>
                    <hs_07am_0859am>average</hs_07am_0859am>
                    <hs_09am_1059am>average</hs_09am_1059am>
                    <hs_11am_1259pm>best</hs_11am_1259pm>
                    <hs_01pm_0259pm>best</hs_01pm_0259pm>
                    <hs_03pm_0459pm>average</hs_03pm_0459pm>
                    <hs_05pm_0659pm>average</hs_05pm_0659pm>
                    <hs_07pm_0859pm>bad</hs_07pm_0859pm>
                    <hs_09pm_1059pm>average</hs_09pm_1059pm>
 */
public class HourStars {
	@Element
	String hs_11pm_1259am;
	@Element
	String hs_01am_0259am;
	@Element
	String hs_03am_0459am;
	@Element
	String hs_05am_0659am;
	@Element
	String hs_07am_0859am;
	@Element
	String hs_09am_1059am;
	@Element
	String hs_11am_1259pm;
	@Element
	String hs_01pm_0259pm;
	@Element
	String hs_03pm_0459pm;
	@Element
	String hs_05pm_0659pm;
	@Element
	String hs_07pm_0859pm;
	@Element
	String hs_09pm_1059pm;
	public String get_hs_11pm_1259am(){
		return hs_11pm_1259am;
	}
	public String get_hs_01am_0259am(){
		return hs_01am_0259am;
	}
	public String get_hs_03am_0459am(){
		return hs_03am_0459am;
	}
	public String get_hs_05am_0659am(){
		return hs_05am_0659am;
	}
	public String get_hs_07am_0859am(){
		return hs_07am_0859am;
	}
	public String get_hs_09am_1059am(){
		return hs_09am_1059am;
	}
	public String get_hs_11am_1259pm(){
		return hs_11am_1259pm;
	}
	public String get_hs_01pm_0259pm(){
		return hs_01pm_0259pm;
	}
	public String get_hs_03pm_0459pm(){
		return hs_03pm_0459pm;
	}
	public String get_hs_05pm_0659pm(){
		return hs_05pm_0659pm;
	}
	public String get_hs_07pm_0859pm(){
		return hs_07pm_0859pm;
	}
	public String get_hs_09pm_1059pm(){
		return hs_09pm_1059pm;
	}
	@Override
  	public String toString() {
  		StringBuilder builder = new StringBuilder();
  		
  		
  		builder.append("\nhs_11pm_1259am\n");
  		builder.append(hs_11pm_1259am);
  		builder.append("\nhs_01am_0259am\n");
  		builder.append(hs_01am_0259am);
  		builder.append("\nhs_03am_0459am\n");
  		builder.append(hs_03am_0459am);
  		builder.append("\nhs_05am_0659am\n");
  		builder.append(hs_05am_0659am);
  		builder.append("\nhs_07am_0859am\n");
  		builder.append(hs_07am_0859am);
  		builder.append("\nhs_09am_1059am\n");
  		builder.append(hs_09am_1059am);
  		builder.append("\nhs_11am_1259pm\n");
  		builder.append(hs_11am_1259pm);
  		builder.append("\nhs_01pm_0259pm\n");
  		builder.append(hs_01pm_0259pm);
  		builder.append("\nhs_03pm_0459pm\n");
  		builder.append(hs_03pm_0459pm);
  		builder.append("\nhs_05pm_0659pm\n");
  		builder.append(hs_05pm_0659pm);
  		builder.append("\nhs_07pm_0859pm\n");
  		builder.append(hs_07pm_0859pm);
  		builder.append("\nhs_09pm_1059pm\n");
  		builder.append(hs_09pm_1059pm);
  		return builder.toString();
  	}
}
