package com.hofs.fengshuicalendar.models;

import java.util.List;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

public class Data {
	
		@Element
		String expiry_date;
		
		@ElementList
		private List<DayBreaker> day_breaker_list;
		
		@ElementList
		private List<DayOfficer> day_officer_list;
		
		@ElementList
		private List<DayStars> day_stars_list;
		
		@ElementList
		private List<Direction> direction_list;
		
		
		@Element
		private SuitAvoidList  suit_avoid_list;
		
		@ElementList
		private List<YBBelt> ybbelt_list;
		
			@ElementList
		private List<Almanac> almanac_list;
		
		 public String getDate() {
		      return expiry_date;
		   }

		 public List<DayBreaker> getDayBreakerList() {
		      return day_breaker_list;
		 }
		 
		 public List<DayOfficer> getDayOfficerList() {
		      return day_officer_list;
		 }
		 public List<DayStars> getDayStarsList() {
		      return day_stars_list;
		 }
		 public List<Direction> getDirectionList() {
		      return direction_list;
		 }
		 public SuitAvoidList getSuitAvoidList() {
		      return suit_avoid_list;
		 }
		 
		
		 
		 public List<YBBelt> getYBBeltList() {
		      return ybbelt_list;
		 }
		 public List<Almanac> getAlmanacList() {
		      return almanac_list;
		 }
	 
	 
	 
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			
			builder.append("\nex_date:\n");
			builder.append(expiry_date);
			builder.append("\ndaybreakerlist");
			builder.append(day_breaker_list);
			builder.append("\ndayofficerlist");
			builder.append(day_officer_list);
			builder.append("\ndaystarslist");
			builder.append(day_stars_list);
			builder.append("\ndirectionlist");
			builder.append(direction_list);
			builder.append("\nsuit_avoid_list");
			builder.append(suit_avoid_list);
			builder.append("\nybbelt_list");
			builder.append(ybbelt_list);
			builder.append("\nalmanac_list");
			builder.append(almanac_list);
			return builder.toString();
		}
		

}
