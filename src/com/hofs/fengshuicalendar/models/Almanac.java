package com.hofs.fengshuicalendar.models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementArray;

import com.hofs.fengshuicalendar.dashboard.storage.Preferences;


public class Almanac {
	@Attribute
	int id;
	
	@Element
	String date_en;
	
	@Element
	String date_cn;
	
	@Element
	String rating;
	
	@Element
	int day_officer;
	@Element
	int dong_gong;
	@Element
	int yb_belt;
	
	@Element(required=false,data=true)
	String special_info_en;
	@Element(required=false,data=true)
	String special_info_cn;
	
	@Element
	int nobleman_direction;
	@Element
	int happiness_direction;
	@Element
	int wealth_direction;
	@Element
	int day_breaker;
	@Element(required=false,data=true)
	String suit;
	@Element
	String avoid;
	@Element(required=false,data=true)
	String day_stars;

	@Element
	HourStars hour_stars;
	
	public int getId(){
		return id;
	}
	public String getDateEn(){
		return date_en;
	}
	public String getDateCn(){
		return date_cn;
	}
	public String getRating(){
		return rating;
	}
	public int getDayOfficer(){
		return day_officer;
	}
	public int getDongGong(){
		return dong_gong;
	}
	public int getYBBelt(){
		return yb_belt;
	}
	public String getSpecialInfoEn(){
		return special_info_en;
	}
	public String getSpecialInfoCn(){
		return special_info_cn;
	}
	public int getNobleManDirection(){
		return nobleman_direction;
	}
	public int getHappinessDirection(){
		return happiness_direction;
	}
	public int getWealthDirection(){
		return wealth_direction;
	}
	public int getDayBreaker(){
		return day_breaker;
	}
	public String getSuit(){
		return suit;
	}
	public String getAvoid(){
		return avoid;
	}
	public String getDayStars(){
		return day_stars;
	}
	public HourStars getHourStars(){
		return hour_stars;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		
		builder.append("\nid\n");
		builder.append(id);
		builder.append("\ndate_en\n");
		builder.append(date_en);
		builder.append("\ndate_cn\n");
		builder.append(date_cn);
		builder.append("\nrating\n");
			builder.append(rating);
			builder.append("\nday_officer\n");
			builder.append(day_officer);
			builder.append("\ndong_gong\n");
			builder.append(dong_gong);
			builder.append("\nyb_belt\n");
		builder.append(yb_belt);
		builder.append("\nspecial_info_en\n");
		builder.append(special_info_en);
		builder.append("\nspecial_info_cn\n");
		builder.append(special_info_cn);
		builder.append("\nnobleman_direction\n");
		builder.append(nobleman_direction);
		builder.append("\nhappiness_direction\n");
		builder.append(happiness_direction);
		builder.append("\nwealth_direction\n");
		builder.append(wealth_direction);
		builder.append("\nday_breaker\n");
		builder.append(day_breaker);
			builder.append("\nsuit\n");
		builder.append(suit);
	builder.append("\navoid\n");
		
		builder.append(avoid);
		builder.append("\nday_stars\n");
		builder.append(day_stars);
	builder.append(hour_stars);
		
		return builder.toString();
	}


}
