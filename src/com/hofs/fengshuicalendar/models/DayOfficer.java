package com.hofs.fengshuicalendar.models;

import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

public class DayOfficer {
	@Attribute
	int id;
	
	@Element(data=true)
	String name_en;
	
	@Element(data=true)
	String name_cn;
	
	@Element(data=true)
	String status;
	
	 public int getId() {
	      return id;
	   }

	   public String getName_en() {
	      return name_en;
	   }
	   public String getName_cn() {
		      return name_cn;
		   }
	   public String getStatus() {
		      return status;
		   }
	@Override
  	public String toString() {
  		StringBuilder builder = new StringBuilder();
  		
  		
  		builder.append("\nid\n");
  		builder.append(id);
  		builder.append("\nName_En\n");
  		builder.append(name_en);
  		builder.append("\nName_Cn");
  		builder.append(name_cn);
  		
  		return builder.toString();
  	}
}
