package com.hofs.fengshuicalendar.models;

import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

public class Category {
	@Attribute
	int id;
	
	@Element(data=true)
	String name_en;
	
	@Element(data=true)
	String name_cn;
	
	@Element(required=false,data=true)
	String description_en;
	
	@Element(required=false,data=true)
	String description_cn;
	
	@ElementList
	private List<SuitAvoid> suit_avoids;
	
	 public int getId() {
	      return id;
	 }
	
	
	public String getName_en() {
	      return name_en;
	}
	public String getName_cn() {
		      return name_cn;
	}
	
	public String getDescription_en() {
	      return description_en;
	}
	public String getDescription_cn() {
	      return description_cn;
	}
	public List<SuitAvoid> getSuitAvoid() {
	      return suit_avoids;
	 }
	@Override
  	public String toString() {
  		StringBuilder builder = new StringBuilder();
  		
  		
  		builder.append("\nid\n");
  		builder.append(id);
  		builder.append("\nName_En\n");
  		builder.append(name_en);
  		builder.append("\nName_Cn");
  		builder.append(name_cn);
  		builder.append("\nDescription_En\n");
  		builder.append(description_en);
  		builder.append("\nDescription_Cn");
  		builder.append(description_cn);
  		builder.append("\nsuit_avoids");
		builder.append(suit_avoids);
  		
  		return builder.toString();
  	}
}
