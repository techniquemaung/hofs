package com.hofs.fengshuicalendar.models;

import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

public class SuitAvoid {
	@Attribute
	int id;
	
	@Element(data=true)
	String name_en;
	
	@Element(data=true)
	String name_cn;
	
	@Element
	int status;
	
	 public int getId() {
	      return id;
	   }
	 public int getStatus() {
	      return status;
	   }

	   public String getName_en() {
	      return name_en;
	   }
	   public String getName_cn() {
		      return name_cn;
		   }
	@Override
  	public String toString() {
  		StringBuilder builder = new StringBuilder();
  		
  		
  		builder.append("\nid\n");
  		builder.append(id);
  		builder.append("\nName_En\n");
  		builder.append(name_en);
  		builder.append("\nName_Cn");
  		builder.append(name_cn);
  		builder.append("\nstatus\n");
  		builder.append(status);
  		return builder.toString();
  	}
}
