package com.hofs.fengshuicalendar.models;

import org.simpleframework.xml.Element;

public class Download {
	@Element(name="location",data=false,required=false )
	String location;
	public String getLocation(){
		return location;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		
		builder.append("\nlocation:");
		builder.append(location);
		return builder.toString();
	}
}
