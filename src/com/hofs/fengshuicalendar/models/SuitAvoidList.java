package com.hofs.fengshuicalendar.models;



import java.util.List;

import org.simpleframework.xml.ElementList;

public class SuitAvoidList {
	@ElementList
	   private List<Category> categories;
	
	 public List<Category> getProperties() {
	      return categories;
	   }
	 @Override
	  	public String toString() {
	  		StringBuilder builder = new StringBuilder();
	  		
	  		
	  		builder.append("\ncategories\n");
	  		builder.append(categories);
	  		
	  		return builder.toString();
	  	}
}
