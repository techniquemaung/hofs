package com.hofs.fengshuicalendar.login.eng;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.hofs.fengshuicalendar.R;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class ZodiacSign extends Activity {

JSONArray jsonarray;
String str="";


TextView name;
@Override
public void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);

setContentView(R.layout.zodiac_sign);

name=(TextView)findViewById(R.id.name);
str=getIntent().getStringExtra("str1");
try {
	SimpleDateFormat input = new SimpleDateFormat("dd/MM/yy");
	SimpleDateFormat output = new SimpleDateFormat("dd MMM yyyy");
	Date date = input.parse(getIntent().getStringExtra("str1"));                 // parse input 
	name.append(output.format(date)+"\n");    // format output
} catch (ParseException e) {
e.printStackTrace();
}

/*InputStream is = getResources().openRawResource(R.raw.sample);*/
InputStream is = null;
try {
	//is = this.getAssets().open("sample.json");
	is = this.getAssets().open("zodiac.json");
} catch (IOException e1) {
	// TODO Auto-generated catch block
	e1.printStackTrace();
}
Writer writer = new StringWriter();
char[] buffer = new char[1024];
try {
    Reader reader=null;
	try {
		
		reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    int n;
    try {
		while ((n = reader.read(buffer)) != -1) {
		    writer.write(buffer, 0, n);
		}
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
} finally {
    try {
		is.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}


 final String JSON_STRING = writer.toString();
 

try {
	jsonarray = new JSONArray(JSON_STRING);
} catch (JSONException e1) {
	// TODO Auto-generated catch block
	e1.printStackTrace();
}
String[] startArr= new String[jsonarray.length()];
String[] endArr=new String[jsonarray.length()];
String[] zodiac_cnArr=new String[jsonarray.length()];
String[] zodiac_enArr=new String[jsonarray.length()];

try{
 for(int i=0; i<jsonarray.length(); i++){
     JSONObject obj = jsonarray.getJSONObject(i);

     endArr[i] = obj.getString("end");
     startArr[i] = obj.getString("start");
     zodiac_cnArr[i]=obj.getString("zodiac_cn");
     zodiac_enArr[i]=obj.getString("zodiac_en");
     
     /* String  end = obj.getString("end");
    String start = obj.getString("start");
    String zodiac_cn=obj.getString("zodiac_cn");
    String zodiac_en=obj.getString("zodiac_en");
    name.append("end"+ "  "+end +"\n"+"start"+"  "+start+"\n"+"zodiac_cn"+"  "+zodiac_cn+"\n"+"zodiac_en"+"  "+zodiac_en+"\n.................\n");*/
    /*endArr[i] = end;
    startArr[i] =start;
    zodiac_cnArr[i]=zodiac_cn;
    zodiac_enArr[i]=zodiac_en;*/
    
	 
 }   
 }
 catch(JSONException e){
	  e.printStackTrace();
 }catch (Exception e) {
 e.printStackTrace();
 }

//name.append("end"+ "  "+endArr[i] +"\n"+"start"+"  "+startArr[i]+"\n"+"zodiac_cn"+"  "+zodiac_cnArr[i]+"\n"+"zodiac_en"+"  "+zodiac_enArr[i]+"\n.................\n");	
int x=0;
//name.append(start[0]+"\n");
while (x<startArr.length){
	
	try {
		SimpleDateFormat input = new SimpleDateFormat("dd/MM/yy");
		SimpleDateFormat output = new SimpleDateFormat("dd MMM yyyy");
		Date date = input.parse(endArr[x]);                 // parse input 
		name.append(output.format(date));    // format output
	} catch (ParseException e) {
	e.printStackTrace();
	}
	//name.append("start"+ "  "+endArr[x] +"\n" );
	 x++;
	 
 }
}
}