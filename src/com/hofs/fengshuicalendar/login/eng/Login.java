package com.hofs.fengshuicalendar.login.eng;



import com.hofs.fengshuicalendar.R;
import com.hofs.fengshuicalendar.dashboard.DashBoardActivity;
import com.hofs.fengshuicalendar.datepicker.DatePickerDailog;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import android.app.Activity; 
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent; 
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle; 
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends Activity {
	int request_Code=1;
	
	/** Called when the activity is first created.
	 
	 ** 
	  */
	Button enter;
	EditText et;
	DatePicker datepicker;
	private int year;
	private int month;
	private int day;
	String str="kyaw";
	static final int DATE_DIALOG_ID = 999;
	 private static final int REQUEST_CODE = 10;
	
	 
	@Override
	public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.login_eng);
	
	
	 enter=(Button)findViewById(R.id.enter);
     et=(EditText)findViewById(R.id.date);
     enter.setVisibility(View.INVISIBLE);
     enter.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
				
				Intent i = new
						Intent("com.hofs.fengshuicalendar.login.eng.Zodiac"); 
						
						i.putExtra("str1", et.getText().toString());
						
						startActivityForResult(i, 1); 
		}
		});
     et.setOnClickListener(new OnClickListener(){
		public void onClick(final View v){
			 enter.setVisibility(View.VISIBLE);
			/*showDialog(DATE_DIALOG_ID);*/
			 final Calendar dateandtime;
         	dateandtime = Calendar.getInstance(Locale.US);
         	DatePickerDailog dp = new DatePickerDailog(Login.this,
						dateandtime, new DatePickerDailog.DatePickerListner() {

							@Override
							public void OnDoneButton(Dialog datedialog, Calendar c) {
								datedialog.dismiss();
								dateandtime.set(Calendar.YEAR, c.get(Calendar.YEAR));
								dateandtime.set(Calendar.MONTH,
										c.get(Calendar.MONTH));
								dateandtime.set(Calendar.DAY_OF_MONTH,
										c.get(Calendar.DAY_OF_MONTH));
								((EditText)v).setText(new SimpleDateFormat("MMMM dd yyyy")
										.format(c.getTime()));
							}

							@Override
							public void OnCancelButton(Dialog datedialog) {
								// TODO Auto-generated method stub
								datedialog.dismiss();
							}
						});
				dp.show();
				
			
	}
	});
     /*  datepicker=(DatePicker)findViewById(R.id.datePicker1);
     final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);*/

	
    
	}
	
	/*
	 @Override
 	protected Dialog onCreateDialog(int id) {
 		switch (id) {
 		case DATE_DIALOG_ID:
 			// set date picker as current date
 			return new DatePickerDialog(this, datePickerListener, year, month,
 					day);
 		}
 		return null;
 	}
	 private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

			// when dialog box is closed, below method will be called.
			public void onDateSet(DatePicker view, int selectedYear,
					int selectedMonth, int selectedDay) {
				year = selectedYear;
				month = selectedMonth;
				day = selectedDay;

				// set selected date into textview
				et.setPadding(160, 0, 0, 0);
				et.setText(new StringBuilder()
						.append(day).append("/").append(month + 1).append("/").append(year)
						.append(" "));

				// set selected date into datepicker also
				datepicker.init(year, month, day, null);

			}
		};
	*/	
	
}