package com.hofs.fengshuicalendar.login.eng;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;



import com.hofs.fengshuicalendar.R;
import com.hofs.fengshuicalendar.api.HofsApi;

import com.hofs.fengshuicalendar.dashboard.storage.InternalStorage;
import com.hofs.fengshuicalendar.dashboard.storage.Preferences;

import com.hofs.fengshuicalendar.xmlparser.XmlParser;
import com.hofs.fengshuicalendar.xmlparser.XmlStringReader;

import com.hofs.fengshuicalendar.models.DayBreaker;
import com.hofs.fengshuicalendar.models.Result;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;



public class Account extends Activity{
	
	ProgressDialog pDialog;
	String str,email,user_id="",mac_address="00:33:55:66",receipt="",pkg_type="pkg-free",api_key="9mZsuASrnQpqZvHjcg86jX58TttbgiVm";
	String URL = "http://dev2.appvantage.sg/app_hofs/index.php?r=almanac/xml";
	private DefaultHttpClient client = new DefaultHttpClient();
	ImageView next,uncheck;
	TextView checktext;
	EditText et;
	String strReceive;
	HttpResponse response;
	Result osd ;
	Preferences p;
	HofsApi api;
	String preString;
	InternalStorage st;
	XmlStringReader xreader;
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acc_login);
       
        
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
      
        strReceive=getIntent().getStringExtra("str");
        xreader=new XmlStringReader(this);
        api=new HofsApi();
        p=new Preferences(this);
    	p.clear();
        st=new InternalStorage(this);
        next=(ImageView)findViewById(R.id.imageView2);
       
        uncheck=(ImageView)findViewById(R.id.imageView3);
        et=(EditText)findViewById(R.id.editText1);
       
        checktext=(TextView)findViewById(R.id.datetv);
        checktext.setText("Sign Up for Feng Shui House \nNewsletters");
       
        et.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				
				et.setText("");
				
			}
        	
        });
       uncheck.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				
				uncheck.setBackgroundResource(R.drawable.check);
				
			}
        	
        });

        next.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				
				if(et.getText().toString().contains("@"))
				{	
					email=et.getText().toString();
					
					if(hasConnection(getApplicationContext())==true){
					new DownloadXML().execute(URL);
						/*Intent intent = new Intent(getApplicationContext(),DownloadActivity.class);
						
						startActivity(intent);*/
						
					}
					if(hasConnection(getApplicationContext())==false)  {
					
    			  	AlertDialog.Builder alertDialog = new AlertDialog.Builder(Account.this);
    		 		 
    		         
    		         alertDialog.setTitle("FengShuiCalendar ");
    		  
    		         
    		         alertDialog.setMessage(" No Internet Connection ");
    		  
    		         
    		         alertDialog.setIcon(R.drawable.ic_launcher);
    		    
    		         
    		         
    		         alertDialog.setPositiveButton("OK ", new DialogInterface.OnClickListener() {
    		             public void onClick(DialogInterface dialog,int which) {
    		            	 preString=xreader.getXmlString();
    		            	 String tagXml="Hofs Data (xml)";
    						 	
    						 	longInfo(tagXml,preString);
    						 	p.set("result",preString);
    		            	 Intent i = new
    		     					Intent("com.hofs.fengshuicalendar.dashboard.MainDashBoardActivity"); 
    		     								
    		     								i.putExtra("str1", et.getText().toString());
    		     								i.putExtra("str2", strReceive);
    		     								
    		     								startActivityForResult(i, 1);
    		            	/* Intent intent = new Intent(getApplicationContext(),DownloadActivity.class);
    							
    							startActivity(intent);*/
    							
    		             }
    		         });
    		  
    		      
    		         alertDialog.show(); 

    					return;
					}
				
				}
				else
    			{
    					
    			  	AlertDialog.Builder alertDialog = new AlertDialog.Builder(Account.this);
    		 		 
    		         
    		         alertDialog.setTitle("FengShuiCalendar ");
    		  
    		        
    		         alertDialog.setMessage(" Type your email address..");
    		  
    		         
    		         alertDialog.setIcon(R.drawable.ic_launcher);
    		    
    		         
    		        
    		         alertDialog.setPositiveButton("OK ", new DialogInterface.OnClickListener() {
    		             public void onClick(DialogInterface dialog,int which) {
    		            	 
    		            
    		             }
    		         });
    		  
    		      
    		         alertDialog.show(); 

    					return;
    			}
			}
        	
        });
      
}
	

		
		private class DownloadXML extends AsyncTask<String, Void, Void> {

			@Override
			protected void onPreExecute() {
				super.onPreExecute();
			
				
				pDialog = new ProgressDialog(Account.this);
				
				pDialog.setTitle("Hofs");
				
				pDialog.setMessage("Loading...");
				pDialog.setIndeterminate(false);
				
				pDialog.show();
			}

			@Override
			protected Void doInBackground(String... Url) {
			
				 try {  
					 	/*preString=xreader.getXmlString();*/
					 	
					 	
					 	api.setParameter(email, user_id, mac_address, receipt,pkg_type, api_key);
					 	preString=api.retrieve(Url[0]);
					 	
						
					 /*	XmlParser parser=new XmlParser(preString);
					 	parser.parse();*/
					 	
					 	String tagXml="Hofs Data (xml)";
					 	
					 	longInfo(tagXml,preString);
					 	p.set("result",preString);
						
						
					} 
			        catch (Exception e) {
			        	Toast.makeText(null, "Error Occured:"+e.getMessage()+"\nerror:"+e.toString(), Toast.LENGTH_LONG).show();
			        	//tv.setText("Error Occured:"+e.getMessage()+"\nerror:"+e.toString());
					}
			        
				
				
				return null;

			}
			
			@Override
			protected void onPostExecute(Void args) {

				
				pDialog.dismiss();
				
					Intent i = new
					Intent("com.hofs.fengshuicalendar.dashboard.MainDashBoardActivity"); 
								
								i.putExtra("str1", et.getText().toString());
								i.putExtra("str2", strReceive);
								
								startActivityForResult(i, 1);
			
				
			}
		}
		
	
		public static void longInfo(String logheadString,String  str) {
			String tag=logheadString;
		    if(str.length() > 4000) {
		        Log.i(tag,str.substring(0, 4000));
		        longInfo(str.substring(4000),tag);
		    } else
		        Log.i(tag,str);
		}
		
		  public static boolean hasConnection(Context c) {
		    ConnectivityManager cm = (ConnectivityManager) c.getSystemService(
		        Context.CONNECTIVITY_SERVICE);

		    NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		    if (wifiNetwork != null && wifiNetwork.isConnected()) {
		      return true;
		    }

		    NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		    if (mobileNetwork != null && mobileNetwork.isConnected()) {
		      return true;
		    }

		    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		    if (activeNetwork != null && activeNetwork.isConnected()) {
		      return true;
		    }

		    return false;
		  }
		
}