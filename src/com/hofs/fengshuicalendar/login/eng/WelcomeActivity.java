package com.hofs.fengshuicalendar.login.eng;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hofs.fengshuicalendar.R;
import com.hofs.fengshuicalendar.dashboard.DashBoardActivity;
import com.hofs.fengshuicalendar.datepicker.DatePickerDailog;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

public class WelcomeActivity extends Activity{
	TextView tv,dateData,text;
	ImageView please,enter,zodiac,next;
	DatePicker datepicker;
	int year,month,day;
	static final int DATE_DIALOG_ID = 999;
	Date date,currentDate,startDate, endDate;
	Bitmap bitmap;
	JSONArray jsonarray;
	SimpleDateFormat input = new SimpleDateFormat("dd/MM/yy");
	SimpleDateFormat output = new SimpleDateFormat("EEE , dd MMM yyyy");
	
	int i=0,path=0;
	
	int names[]=new int[]{
			R.drawable.img1,
			R.drawable.img2,
			R.drawable.img3,
			R.drawable.img4,
			R.drawable.img5,
			R.drawable.img6,
			R.drawable.img7,
			R.drawable.img8,
			R.drawable.img9,
			R.drawable.img10,
			R.drawable.img11,
			R.drawable.img12
			
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.welcome);
	tv=(TextView)findViewById(R.id.datetv);
	text=(TextView)findViewById(R.id.textView3);
	enter=(ImageView)findViewById(R.id.imageView4);
	please=(ImageView)findViewById(R.id.imageView2);
	zodiac=(ImageView)findViewById(R.id.imageView3);
	next=(ImageView)findViewById(R.id.imageView5);
	
	datepicker = (DatePicker)findViewById(R.id.datePicker1);
	dateData=(TextView)findViewById(R.id.textView2);
	
	InputStream is = null;
	try {
		//is = this.getAssets().open("sample.json");
		is = this.getAssets().open("zodiac.json");
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	Writer writer = new StringWriter();
	char[] buffer = new char[1024];
	try {
	    Reader reader=null;
		try {
			
			reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    int n;
	    try {
			while ((n = reader.read(buffer)) != -1) {
			    writer.write(buffer, 0, n);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} finally {
	    try {
			is.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	 final String JSON_STRING = writer.toString();
	 

	
	
	
	tv.setOnClickListener(new OnClickListener(){
		public void onClick(final View v){
			 enter.setVisibility(View.VISIBLE);
			 text.setVisibility(View.INVISIBLE);
			 next.setVisibility(View.INVISIBLE);
			 please.setVisibility(View.INVISIBLE);
			 /*showDialog(DATE_DIALOG_ID);*/
			 final Calendar dateandtime;
         	dateandtime = Calendar.getInstance(Locale.US);
         	DatePickerDailog dp = new DatePickerDailog(WelcomeActivity.this,
						dateandtime, new DatePickerDailog.DatePickerListner() {

							@Override
							public void OnDoneButton(Dialog datedialog, Calendar c) {
								datedialog.dismiss();
								dateandtime.set(Calendar.YEAR, c.get(Calendar.YEAR));
								dateandtime.set(Calendar.MONTH,
										c.get(Calendar.MONTH));
								dateandtime.set(Calendar.DAY_OF_MONTH,
										c.get(Calendar.DAY_OF_MONTH));
							/*	((TextView)v).setText(new SimpleDateFormat("dd MM yyyy")
										.format(c.getTime()));*/
								 year = c.get(Calendar.YEAR);
									month = c.get(Calendar.MONTH);
									 day = c.get(Calendar.DAY_OF_MONTH);
									 ((TextView)v).setText(new StringBuilder()
								.append(day).append("/").append(month + 1).append("/").append(year)
								.append(" "));
							}

							@Override
							public void OnCancelButton(Dialog datedialog) {
								// TODO Auto-generated method stub
								datedialog.dismiss();
							}
						});
				dp.show();
				
			
	}
	});
	
	enter.setOnClickListener(new OnClickListener(){
		public void onClick(View v){
			
			enter.setVisibility(View.INVISIBLE);
			text.setVisibility(View.VISIBLE);
			tv.setVisibility(View.INVISIBLE);
			next.setVisibility(View.VISIBLE);
			try {
				jsonarray = new JSONArray(JSON_STRING);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
			
			

			try{
			 for(int i=0; i<jsonarray.length(); i++){
			     JSONObject obj = jsonarray.getJSONObject(i);

			    /* endArr[i] = obj.getString("end");
			     startArr[i] = obj.getString("start");
			     zodiac_cnArr[i]=obj.getString("zodiac_cn");
			     zodiac_enArr[i]=obj.getString("zodiac_en");*/
			     
			    String  end_date = obj.getString("end");
			    String start_date = obj.getString("start");
			    String zodiac_cn=obj.getString("zodiac_cn");
			    String zodiac_en=obj.getString("zodiac_en");
			   
			   startDate = (Date)input.parse(start_date); 
			   endDate = (Date)input.parse(end_date);
			   try {
					 
					date = input.parse(tv.getText().toString().trim());
				} catch (ParseException e) {
					
					e.printStackTrace();
				}  
			   if ((date.after(startDate) && date.before(endDate))|| (date.equals(startDate))||(date.equals(endDate))){
				   dateData.append(output.format(date));
			    	
			    	if(i<12){
						//str=keys[i];
						path=names[i];
						zodiac.setBackgroundResource(path);
						}
						if(i>=12){
							int newi=((i)%12);
							
							
						//str=keys[newi];
						path=names[newi];
						//System.out.println( newi);
						zodiac.setBackgroundResource(path);
						text.append( " "+zodiac_en+".\n "+ "Sign of "+zodiac_en.toLowerCase() +"s are elit, self-confidenced .\nThey are big-soul.");
						}
			    	
			    }
			
			   
			    	
			    
				 
			 }   
			 }
			 catch(JSONException e){
				  e.printStackTrace();
			 }catch (Exception e) {
			 e.printStackTrace();
			 }
			
			
			 
			
	}
	});
	
	next.setOnClickListener(new OnClickListener(){
				public void onClick(View v){
							/*Intent in = new Intent(getApplicationContext(),com.hofs.fengshuicalendar.login.eng.Account.class);
						    startActivity(in);*/
					Intent i = new
							Intent("com.hofs.fengshuicalendar.login.eng.Account"); 
					//SimpleDateFormat input = new SimpleDateFormat("dd/MM/yy");
					//SimpleDateFormat output = new SimpleDateFormat("dd MMM yyyy");
					//Date date = input.parse(getIntent().getStringExtra("str1"));                 // parse input 
					//name.append(output.format(date)+"\n");  
							i.putExtra("str", output.format(date));
							
							startActivityForResult(i, 1); 
						       
								
				}
	});

    final Calendar c = Calendar.getInstance();
		 year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		 day = c.get(Calendar.DAY_OF_MONTH);
	
		 
}
	/* @Override
	 	protected Dialog onCreateDialog(int id) {
	 		switch (id) {
	 		case DATE_DIALOG_ID:
	 			// set date picker as current date
	 			return new DatePickerDialog(this, datePickerListener, year, month,
	 					day);
	 		}
	 		return null;
	 	}
		 private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

				// when dialog box is closed, below method will be called.
				public void onDateSet(DatePicker view, int selectedYear,
						int selectedMonth, int selectedDay) {
					year = selectedYear;
					month = selectedMonth;
					day = selectedDay;

					// set selected date into textview
					//tv.setPadding(160, 0, 0, 0);
					tv.setText(new StringBuilder()
							.append(day).append("/").append(month + 1).append("/").append(year)
							.append(" "));
					           
						

					// set selected date into datepicker also
					datepicker.init(year, month, day, null);

				}
			};*/
}
