package com.hofs.fengshuicalendar.api;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import com.hofs.fengshuicalendar.xmlparser.XmlParser;
import com.hofs.fengshuicalendar.models.Result;


import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

public class HofsApi extends Activity{
	
	String email,user_id,mac_address,receipt,pkg_type,api_key;
	
	private DefaultHttpClient client = new DefaultHttpClient();
	
	public String retrieve(String url) {

		HttpPost postRequest = new HttpPost(url);
		
		List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);
		nameValuePair.add(new BasicNameValuePair("email",this.email
				));
		
		nameValuePair.add(new BasicNameValuePair("user_id",this.user_id
				));
		nameValuePair.add(new BasicNameValuePair("mac_address",this.mac_address
				));
		nameValuePair.add(new BasicNameValuePair("package_type",this.pkg_type
				));
		nameValuePair.add(new BasicNameValuePair("receipt",this.receipt
				));
		nameValuePair.add(new BasicNameValuePair("api_key",this.api_key));
		
		
		try {
			
			postRequest.setEntity(new UrlEncodedFormEntity(nameValuePair));
		} catch (UnsupportedEncodingException e) {
			
			e.printStackTrace();
		}

		try {

			HttpResponse getResponse = client.execute(postRequest);
			final int statusCode = getResponse.getStatusLine().getStatusCode();

			if (statusCode != HttpStatus.SC_OK) {
				return null;
			}

			HttpEntity getResponseEntity = getResponse.getEntity();

			if (getResponseEntity != null) {
				return EntityUtils.toString(getResponseEntity);
			}

		} 
		catch (IOException e) {
			postRequest.abort();
			Log.w(getClass().getSimpleName(), "Error for URL " + url, e);
		}

		return null;
	
		

	}

	public void setParameter(String mail, String userid, String macaddress,
			String rece,String pkgtype, String apikey) {
		// TODO Auto-generated method stub
		this.email=mail;
		this.user_id=userid;
		this.mac_address=macaddress;
		this.receipt=rece;
		this.pkg_type=pkgtype;
		this.api_key=apikey;
	}
	
	

}
