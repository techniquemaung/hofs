package com.hofs.fengshuicalendar.dashboard;




import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.hofs.fengshuicalendar.R;
import com.hofs.fengshuicalendar.dashboard.fragments.FragmentButton;
import com.hofs.fengshuicalendar.dashboard.fragments.FragmentChangeDateOfBirth;

import com.hofs.fengshuicalendar.dashboard.fragments.FragmentDatePicker;

import com.hofs.fengshuicalendar.dashboard.fragments.FragmentImageView;
import com.hofs.fengshuicalendar.dashboard.fragments.FragmentLanguage;
import com.hofs.fengshuicalendar.dashboard.fragments.FragmentListView;
import com.hofs.fengshuicalendar.dashboard.fragments.FragmentMain;
import com.hofs.fengshuicalendar.dashboard.fragments.FragmentRadioButton;
import com.hofs.fengshuicalendar.dashboard.fragments.FragmentSearch;
import com.hofs.fengshuicalendar.dashboard.fragments.FragmentTextView;
import com.hofs.fengshuicalendar.dashboard.fragments.FragmentToggleButton;
import com.hofs.fengshuicalendar.dashboard.fragments.FragmentWebView;
import com.hofs.fengshuicalendar.dashboard.layout.MainLayout;
import com.hofs.fengshuicalendar.dashboard.storage.InternalStorage;
import com.hofs.fengshuicalendar.dashboard.storage.Preferences;
import com.hofs.fengshuicalendar.datepicker.DatePickerDailog;
import com.hofs.fengshuicalendar.xmlparser.XmlParser;
import com.hofs.fengshuicalendar.xmlparser.XmlStringReader;

import com.hofs.fengshuicalendar.models.DayBreaker;
import com.hofs.fengshuicalendar.models.Result;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

// DK 
// This is my studying about Sliding Menu following Youtube video
public class DashBoardActivity extends FragmentActivity {

    // The MainLayout which will hold both the sliding menu and our main content
    // Main content will holds our Fragment respectively
    MainLayout mainLayout;
    
    // ListView menu
    private ListView lvMenu;
    private String[] lvMenuItems;
    
    // Menu button
    ImageView btMenu;
   // XmlStringReader xreader;
    // Title according to fragment
    TextView tvTitle,date,tvxml;
    public static String str,strDate;
    Preferences p;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // Inflate the mainLayout
        mainLayout = (MainLayout)this.getLayoutInflater().inflate(R.layout.dashboard, null);
        
        setContentView(mainLayout);
        
        str=getIntent().getStringExtra("str1");
        strDate=getIntent().getStringExtra("str2");
        // Init menu
    
        lvMenuItems = getResources().getStringArray(R.array.menu_items);
        
        int i=lvMenuItems.length;
        lvMenuItems[i-1]=str;
        lvMenu = (ListView) findViewById(R.id.activity_main_menu_listview);
        lvMenu.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, lvMenuItems));
        lvMenu.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onMenuItemClick(parent, view, position, id);
            }
            
        });
       
        
        // Get menu button
        btMenu = (ImageView) findViewById(R.id.activity_main_content_button_menu);
        btMenu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Show/hide the menu
                toggleMenu(v);
            }
        });
        
        // Get title textview
        tvTitle = (TextView) findViewById(R.id.activity_main_content_title);
       
     
       
       
       
        date = (TextView) findViewById(R.id.datetv);
        date.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
               /* 
            	FragmentManager fm = DashBoardActivity.this.getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                Fragment fragment = null;
            	 fragment = new FragmentDatePicker();
            	 if(fragment != null) {
                   
                     ft.replace(R.id.activity_main_content_fragment, fragment);
                     ft.commit();
                     
                  
                     tvTitle.setText("date click");
                 }
                 
                */
            	final Calendar dateandtime;
            	dateandtime = Calendar.getInstance(Locale.US);
            	DatePickerDailog dp = new DatePickerDailog(DashBoardActivity.this,
						dateandtime, new DatePickerDailog.DatePickerListner() {

							@Override
							public void OnDoneButton(Dialog datedialog, Calendar c) {
								datedialog.dismiss();
								dateandtime.set(Calendar.YEAR, c.get(Calendar.YEAR));
								dateandtime.set(Calendar.MONTH,
										c.get(Calendar.MONTH));
								dateandtime.set(Calendar.DAY_OF_MONTH,
										c.get(Calendar.DAY_OF_MONTH));
							/*	((TextView)v).setText(new SimpleDateFormat("dd MMM yyyy")
										.format(c.getTime()));*/
								FragmentManager fm = DashBoardActivity.this.getSupportFragmentManager();
							    FragmentTransaction ft = fm.beginTransaction();
							        tvTitle.setText("HOFS");
							    FragmentMain fragment = new FragmentMain();
							
							    //fragment.saveDataToPreferences(getApplicationContext());
							 
							    ft.add(R.id.activity_main_content_fragment, fragment);
							        
							       
							   /* fragment.datetext.setText("\n"+new SimpleDateFormat("EEE , dd MMM yyyy")
										.format(c.getTime()));*/
							    ft.commit();
							}

							@Override
							public void OnCancelButton(Dialog datedialog) {
								// TODO Auto-generated method stub
								datedialog.dismiss();

	    		            	 Intent intent = new Intent(getApplicationContext(),MainDashBoardActivity.class);
	    							
	    							startActivity(intent);
							}
						});
				dp.show();
				
				
				
				
			}
		});
      //  FragmentMain.datetext.setText(strDate);
          
        // Add FragmentMain as the initial fragment       
   
        FragmentManager fm = DashBoardActivity.this.getSupportFragmentManager();
        
        FragmentTransaction ft = fm.beginTransaction();
        
        FragmentMain main = new FragmentMain();
        
      //  main.saveDataToPreferences(this);
        
        
        ft.add(R.id.activity_main_content_fragment, main);
        
        ft.commit();  
        
       
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void toggleMenu(View v){
        mainLayout.toggleMenu();
    }
    
    // Perform action when a menu item is clicked
    private void onMenuItemClick(AdapterView<?> parent, View view, int position, long id) {
        String selectedItem = lvMenuItems[position];
        String currentItem = tvTitle.getText().toString();
        
        // Do nothing if selectedItem is currentItem
        if(selectedItem.compareTo(currentItem) == 0) {
            mainLayout.toggleMenu();
            return;
        }
          
        FragmentManager fm = DashBoardActivity.this.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = null;
      
       
        
        
       /* if(selectedItem.compareTo("Today's Predict") == 0) {
            fragment = new FragmentMain();
        }*/
     /*   if(selectedItem.compareTo("Search for Good Dates") == 0) {
            fragment = new FragmentSearch();
              
        } else if(selectedItem.compareTo("Change Date Of Birth") == 0) {
            fragment = new FragmentChangeDateOfBirth();
        } else if(selectedItem.compareTo("Language") == 0) {
            fragment = new FragmentLanguage();
        } else if(selectedItem.compareTo("In-App Purchase") == 0) {
            fragment = new FragmentImageView();
        } else if(selectedItem.compareTo("Consultation Form") == 0) {
            fragment = new FragmentListView();
        } else if(selectedItem.compareTo("Feedback Form") == 0) {
            fragment = new FragmentRadioButton();
        } else if(selectedItem.compareTo("Glossary") == 0) {
            fragment = new FragmentTextView();
            Bundle b = new Bundle();
            b.putString("KEY_STRING", "Please display this text");
            fragment.setArguments(b);
        } else if(selectedItem.compareTo("Introduction to App") == 0) {
            fragment = new FragmentToggleButton();
        } else if(selectedItem.compareTo("gmail") == 0) {
            fragment = new FragmentWebView();
        }*/
         if(selectedItem.compareTo("Day Breaker List") == 0) {
        	 
        	
          fragment = new FragmentSearch();
           // fragment = new FragmentMain();
           
            
        } else if(selectedItem.compareTo("Day Officer List") == 0) {
            fragment = new FragmentChangeDateOfBirth();
        }
        else if(selectedItem.compareTo("Day Stars List") == 0) {
            fragment = new FragmentLanguage();
        } else if(selectedItem.compareTo("Direction List") == 0) {
            fragment = new FragmentImageView();
        } else if(selectedItem.compareTo("Suit Aovid List") == 0) {
            fragment = new FragmentListView();
        } else if(selectedItem.compareTo("YB belt list") == 0) {
            fragment = new FragmentRadioButton();
        } else if(selectedItem.compareTo("Almanac list") == 0) {
            fragment = new FragmentTextView();
           
        } else if(selectedItem.compareTo("Introduction to App") == 0) {
            fragment = new FragmentToggleButton();
        } else if(selectedItem.compareTo("gmail") == 0) {
            fragment = new FragmentWebView();
        }
       
        if(fragment != null) {
            // Replace current fragment by this new one
        	//ft.replace(R.id.activity_main_content_fragment2, fragment);
            ft.replace(R.id.activity_main_content_fragment, fragment);
            ft.commit();
            
            // Set title accordingly
            tvTitle.setText(selectedItem);
        }
        
        // Hide menu anyway
        mainLayout.toggleMenu();
    }
    
    @Override
    public void onBackPressed() {
        if (mainLayout.isMenuShown()) {
            mainLayout.toggleMenu();
        }
        else {
            super.onBackPressed();
        }
    }
}
