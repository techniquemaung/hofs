package com.hofs.fengshuicalendar.dashboard;





import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import java.util.Locale;

import com.hofs.fengshuicalendar.R;

import com.hofs.fengshuicalendar.dashboard.mainfragments.FragmentMain2;
import com.hofs.fengshuicalendar.dashboard.mainfragments.MenuListFragment;



import com.hofs.fengshuicalendar.dashboard.fragments.FragmentChangeDateOfBirth;
import com.hofs.fengshuicalendar.dashboard.fragments.FragmentImageView;
import com.hofs.fengshuicalendar.dashboard.fragments.FragmentLanguage;
import com.hofs.fengshuicalendar.dashboard.fragments.FragmentListView;
import com.hofs.fengshuicalendar.dashboard.fragments.FragmentMain;
import com.hofs.fengshuicalendar.dashboard.fragments.FragmentRadioButton;
import com.hofs.fengshuicalendar.dashboard.fragments.FragmentSearch;
import com.hofs.fengshuicalendar.dashboard.fragments.FragmentTextView;
import com.hofs.fengshuicalendar.dashboard.fragments.FragmentToggleButton;
import com.hofs.fengshuicalendar.dashboard.fragments.FragmentWebView;
import com.hofs.fengshuicalendar.dashboard.layout.MainDashBoardLayout;


import com.hofs.fengshuicalendar.datepicker.DatePickerDailog;



import android.app.Dialog;
import android.content.Intent;

import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;



import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import android.widget.TextView;


public class MainDashBoardActivity extends FragmentActivity {

   
   public static MainDashBoardLayout mainLayout;
    
    
    private ListView lvMenu;
    private String[] lvMenuItems;
    
  
    public ImageView btMenu;
    public static TextView tvTitle;
    TextView date,tvxml;
    public static String str,strDate;
   
   
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        
    	super.onCreate(savedInstanceState);
        
        mainLayout = (MainDashBoardLayout)this.getLayoutInflater().inflate(R.layout.maindashboard, null);
        setContentView(mainLayout);
        str=getIntent().getStringExtra("str1");
        strDate=getIntent().getStringExtra("str2");
        
       
        
      /*  lvMenuItems = getResources().getStringArray(R.array.mainmenuitems);
    
        //int i=lvMenuItems.length;
       // lvMenuItems[i-1]=str;
       // str=getIntent().getStringExtra("str1");
        //strDate=getIntent().getStringExtra("str2");
        
        lvMenu = (ListView) findViewById(R.id.listmenu);
        lvMenu.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, lvMenuItems));
        lvMenu.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                onMenuItemClick(parent, view, position, id);
            }
            
        });
       
     */
        
        
        tvTitle = (TextView) findViewById(R.id.activity_main_content_title);
        
        btMenu = (ImageView) findViewById(R.id.activity_main_content_button_menu);
        btMenu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
               
                toggleMenu(v);
              /*  FragmentManager fm = MainDashBoardActivity.this.getSupportFragmentManager();
                
                FragmentTransaction ft2 = fm.beginTransaction();
                
                FragmentMain2 main = new FragmentMain2();
                Date dt = new Date();
              
                	Calendar now = Calendar.getInstance();
                	int year = now.get(Calendar.YEAR);
                	int month = now.get(Calendar.MONTH); // Note: zero based!
                	int day = now.get(Calendar.DAY_OF_MONTH);
               
				 	StringBuilder dateForSign=new StringBuilder()
				 	.append(day).append("/").append(month + 1).append("/").append(year)
				 	.append(" ");
				 	Bundle bundle=new Bundle();
			    bundle.putString("DateForShow", new SimpleDateFormat("EEE , dd MMM yyyy").format(dt.getTime()));
               	bundle.putString("DateForSign", dateForSign.toString());
			    bundle.putString("date", new SimpleDateFormat("yyyy-MM-dd").format(dt.getTime()));  
			    
			    main.setArguments(bundle);
                
                ft2.add(R.id.placeforfragment, main);
                
                ft2.commit(); 
                FragmentManager fm = MainDashBoardActivity.this.getSupportFragmentManager();
                
                FragmentTransaction ft2 = fm.beginTransaction();
                
                MenuListFragment menulistFrag=new  MenuListFragment();
                menulistFrag.setMLayout(mainLayout);
                ft2.add(R.id.placeformenufragment, menulistFrag);
                ft2.commit(); */
            }
        });
       
       
       
       
   
       
        date = (TextView) findViewById(R.id.datetv);
        date.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
               
            	final Calendar dateandtime;
            	dateandtime = Calendar.getInstance(Locale.US);
            	DatePickerDailog dp = new DatePickerDailog(MainDashBoardActivity.this,
						dateandtime, new DatePickerDailog.DatePickerListner() {

							@Override
							public void OnDoneButton(Dialog datedialog, Calendar c) {
								datedialog.dismiss();
								dateandtime.set(Calendar.YEAR, c.get(Calendar.YEAR));
								dateandtime.set(Calendar.MONTH,
										c.get(Calendar.MONTH));
								dateandtime.set(Calendar.DAY_OF_MONTH,
										c.get(Calendar.DAY_OF_MONTH));
								
								FragmentManager fm2 = MainDashBoardActivity.this.getSupportFragmentManager();
							    FragmentTransaction ft2 = fm2.beginTransaction();
							    FragmentMain2 frag = new FragmentMain2();
							    
							    //Calendar now = Calendar.getInstance();
					        	int year = c.get(Calendar.YEAR);
					        	int month = c.get(Calendar.MONTH); // Note: zero based!
					        	int day = c.get(Calendar.DAY_OF_MONTH);
					   
					        	StringBuilder dateForSign=new StringBuilder()
					        	.append(day).append("/").append(month + 1).append("/").append(year)
					        	.append(" ");
							    Bundle bundle=new Bundle();
							 
							    bundle.putString("DateForShow",new SimpleDateFormat("EEE , dd MMM yyyy").format(c.getTime()));
							    bundle.putString("DateForSign", dateForSign.toString());
							    bundle.putString("date", new SimpleDateFormat("yyyy-MM-dd").format(c.getTime()));
							    
							    frag.setArguments(bundle);
						       
							    ft2.add(R.id.placeforfragment, frag);
							    tvTitle.setText("TODAY'S PREDICT");    
							  
							    
							    ft2.commit();
							}

							@Override
							public void OnCancelButton(Dialog datedialog) {
								// TODO Auto-generated method stub
								datedialog.dismiss();
								 
								Intent i = new
										Intent("com.hofs.fengshuicalendar.dashboard.DashBoardActivity"); 
													
													i.putExtra("str1", MainDashBoardActivity.str);
													i.putExtra("str2", MainDashBoardActivity.strDate);
													
													startActivityForResult(i, 1);
								

	    		            	
							}
						});
				dp.show();
				
				
				
				
			}
		});
  
       FragmentManager fm = MainDashBoardActivity.this.getSupportFragmentManager();
        
        FragmentTransaction ft2 = fm.beginTransaction();
        
        FragmentMain2 main = new FragmentMain2();
        Date dt = new Date();
        	Calendar now = Calendar.getInstance();
        	int year = now.get(Calendar.YEAR);
        	int month = now.get(Calendar.MONTH); // Note: zero based!
        	int day = now.get(Calendar.DAY_OF_MONTH);
   
        	StringBuilder dateForSign=new StringBuilder()
        	.append(day).append("/").append(month + 1).append("/").append(year)
        	.append(" "); 
              Bundle bundle=new Bundle();
			    bundle.putString("DateForShow", new SimpleDateFormat("EEE , dd MMM yyyy").format(dt.getTime()));
              bundle.putString("DateForSign", dateForSign.toString());
			    bundle.putString("date", new SimpleDateFormat("yyyy-MM-dd").format(dt.getTime()));  
			   
			    main.setArguments(bundle);
    
			   
        ft2.add(R.id.placeforfragment, main);
        tvTitle.setText("TODAY'S PREDICT"); 
       
       MenuListFragment menulistFrag=new  MenuListFragment();
       menulistFrag.setMLayout(mainLayout);
       ft2.add(R.id.placeformenufragment, menulistFrag);
       
       ft2.commit(); 
       
      
    }

   
   
 
    public void toggleMenu(View v){
        mainLayout.toggleMenu();
    }
  
  /*
    private void onMenuItemClick(AdapterView<?> parent, View view, int position, long id) {
        String selectedItem = lvMenuItems[position];
        String currentItem = tvTitle.getText().toString();
        
      
        if(selectedItem.compareTo(currentItem) == 0) {
            mainLayout.toggleMenu();
            return;
        }
        FragmentManager fm = MainDashBoardActivity.this.getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = null;
      
       
        
        
     
         if(selectedItem.compareTo("Search for Good Dates") == 0) {
        	 
        	//fragment=new FragmentMain2();
        	
          fragment = new MenuFragmentSearch();
          
          
            
        } else if(selectedItem.compareTo("Change Date Of Birth") == 0) {
        	fragment = new MenuFragmentSearch();
        }
        else if(selectedItem.compareTo("Language") == 0) {
        	fragment = new MenuFragmentSearch();
        } else if(selectedItem.compareTo("In-App Purchase") == 0) {
        	fragment = new MenuFragmentSearch();
        } else if(selectedItem.compareTo("Consultation Form") == 0) {
        	fragment = new MenuFragmentSearch();
        } else if(selectedItem.compareTo("Feedback Form") == 0) {
        	fragment = new MenuFragmentSearch();
        } else if(selectedItem.compareTo("Glossary") == 0) {
        	fragment = new MenuFragmentSearch();
           
        } else if(selectedItem.compareTo("Introduction to App") == 0) {
        	fragment = new MenuFragmentSearch();
        } 
      
        if(fragment != null) {
           
            ft.replace(R.id.placeforfragment, fragment);
            ft.commit();
            
           
            tvTitle.setText(selectedItem);
        }
        
        mainLayout.toggleMenu();
       
    }
*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (mainLayout.isMenuShown()) {
            mainLayout.toggleMenu();
        }
        else {
            super.onBackPressed();
        }
    }
   
}
