package com.hofs.fengshuicalendar.dashboard.storage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class InternalStorage {
	Context context;
	String FILENAME ="mydataxml.xml";
	
	
	public InternalStorage(Context con){
		this.context=con;
		this.FILENAME=FILENAME;
		
	}
	public boolean empt(String fileName) {

	    File file = this.context.getFileStreamPath(fileName);
	    if(file.exists()){
	        return false;
	    }
	    else return true;
	 }
	public void saveXmlFile(String xml){
		
		
		//String strMsgToSave = xml;
		FileOutputStream fos;
		try
		{
		    fos = this.context.openFileOutput( this.FILENAME, Context.MODE_PRIVATE );
		    try
		    {
		        fos.write( xml.getBytes() );
		        fos.close();

		    }
		    catch (IOException e)
		    {
		        e.printStackTrace();
		    }

		}
		catch (FileNotFoundException e)
		{
		    e.printStackTrace();
		}
		/*	BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(new 
                File(getFilesDir()+File.separator+"MyFile.txt")));
				bufferedWriter.write("lalit poptani");
				bufferedWriter.close();
		 try {
	            File myFile = new File("/sdcard/result.xml");
	            myFile.createNewFile();
	            FileOutputStream fOut = new FileOutputStream(myFile);
	            OutputStreamWriter myOutWriter = 
	                                    new OutputStreamWriter(fOut);
	            myOutWriter.append(xml);
	            myOutWriter.close();
	            fOut.close();
	           
	        } catch (Exception e) {
	        	String tag="Saving xml file error:";
	           Log.d(tag,e.getMessage());
	        }*/
		
	}
	public String readXmlFile(){
		/* try {
	            File myFile = new File("/sdcard/result.xml");
	            FileInputStream fIn = new FileInputStream(myFile);
	            BufferedReader myReader = new BufferedReader(
	                    new InputStreamReader(fIn));
	            String aDataRow = "";
	            String aBuffer = "";
	            while ((aDataRow = myReader.readLine()) != null) {
	                aBuffer += aDataRow + "\n";
	            }
	           return (aBuffer);
	            
	        } catch (Exception e) {
	        	String tag="Saving xml file error:";
		           Log.d(tag,e.getMessage());
		        }
		 return null;*/
		
		int ch;
		StringBuffer fileContent = new StringBuffer("");
		FileInputStream fis;
		try {
		    fis = this.context.openFileInput( this.FILENAME );
		    try {
		        while( (ch = fis.read()) != -1)
		            fileContent.append((char)ch);
		    } catch (IOException e) {
		        e.printStackTrace();
		    }
		} catch (FileNotFoundException e) {
		    e.printStackTrace();
		}

		String data = new String(fileContent);
		return data;
	}
}
