package com.hofs.fengshuicalendar.dashboard.storage;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v4.app.Fragment;

public class Preferences {
    public static final String MY_PREF = "MyPreferences";
    int num;

    public SharedPreferences sharedPreferences=null;
    public Editor editor;

 public Preferences(Context context) {
        this.sharedPreferences = context.getSharedPreferences(MY_PREF, 0);
        this.editor = this.sharedPreferences.edit();
    }
   /*public Preferences(Fragment f){
    	this.sharedPreferences = f.getActivity().getSharedPreferences(MY_PREF, 0);
        this.editor = this.sharedPreferences.edit();
    }*/

    public void set(String key, String value) {
        this.editor.putString(key, value);
        this.editor.commit();
    }
   

    public String get(String key) {
        return this.sharedPreferences.getString(key, "defaultStringIfNothingFound");
    }
    public void SaveInt(String key, int value){
        
        editor.putInt(key, value);
        editor.commit();
    }
    public void LoadInt(){
      
        num= sharedPreferences.getInt("key", 0);
    }
    public void clear(String key) {
        this.editor.remove(key);
        this.editor.commit();
    }

    public void clear() {
        this.editor.clear();
        this.editor.commit();
    }
}