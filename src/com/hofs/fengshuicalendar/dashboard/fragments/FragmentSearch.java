package com.hofs.fengshuicalendar.dashboard.fragments;

import java.util.List;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.hofs.fengshuicalendar.R;
import com.hofs.fengshuicalendar.dashboard.fragments.ImageAdapter2;
import com.hofs.fengshuicalendar.dashboard.storage.Preferences;
import com.hofs.fengshuicalendar.main.SplashScreen;
import com.hofs.fengshuicalendar.xmlparser.XmlParser;

import com.hofs.fengshuicalendar.models.DayBreaker;
import com.hofs.fengshuicalendar.models.DayOfficer;

public class FragmentSearch extends Fragment {
   
 //   ListView listView;
	
    ListView list;
	String string;
	List<DayBreaker> daybreakerList;
	List<DayOfficer> dayofficerList;
	   
    public FragmentSearch() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	
        View view = inflater.inflate(R.layout.fragment_search, null);
       
        saveDataToPreferences(this);
        list = (ListView) view.findViewById(R.id.list);
        String[] id = new String[daybreakerList.size()];
      		int i=0;
      		
      		for (DayBreaker db: daybreakerList) {
      		   id[i++] =Integer.toString(	db.getId())+"\n"+
      				   						"name_en="+db.getName_en()+"\n"+
      				   						"name_cn="+db.getName_cn()+"\n"+
      				   						"three_harmony_1="+db.getThree_harmony_1()+"\n"+
      				   						"three_harmony_2="+db.getThree_harmony_2()+"\n"+
      				   						"three_harmony_3="+db.getThree_harmony_3()+"\n"+
      				   						"six_harmony_1="+db.getSix_harmony_1()+"\n"+
      				   						"six_harmony_2="+db.getSix_harmony_2()+"\n"+
      				   						"auspicious="+db.getAuspicious()+"\n"+
      				   						"inauspicious_hour="+db.getInauspicious_hour();
      		    
      			//id+=Integer.toString(db.getId());
      		}
      		
      		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
      			        android.R.layout.simple_list_item_1, id);
      		list.setAdapter(adapter);
        
        /*
        String[] listItems={
    			"Demolition & Ending Arrangement",
    			"Engagements & Weddings",
    			"Making A Wish & Prayers",
    			"Medical Activities & Attention",
    			"Moving Home, Shifting Office & Relocation",
    			"Opening Ceremony & Initialising",
    			"Others",
    			"Renovation & Construction",
    			"Setting Bed,Altar, Stove & Desk",
    			"Sign Contracts, Purchasing & Financing",
    			"Socialising & Hobbies",
    			"Taking Up New Job & Position",
    			"Travelling & Holidays"}; 
        listView = (ListView) view.findViewById(R.id.list);
        listView.setAdapter(new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, listItems));*/
       // listView.setListAdapter(new ImageAdapter2(this, R.layout.searchlistmenu, R.id.text1, R.id.right_arrow, listItems, listImages ));
        
        
   
	
        return view;
    }
    
    public void saveDataToPreferences(Fragment fragment) {
		// TODO Auto-generated method stub
		
		Preferences p=new Preferences(fragment.getActivity());
		String xml=p.get("result");
	       
	      XmlParser parser=new XmlParser(xml);
	     this.daybreakerList=parser.getDayBreakerList();
	   //   this.dayofficerList=parser.getDayOfficerList();
		
	}
	
  
}
