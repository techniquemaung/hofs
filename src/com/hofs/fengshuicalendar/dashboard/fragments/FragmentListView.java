package com.hofs.fengshuicalendar.dashboard.fragments;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.hofs.fengshuicalendar.R;
import com.hofs.fengshuicalendar.dashboard.storage.Preferences;
import com.hofs.fengshuicalendar.xmlparser.XmlParser;

import com.hofs.fengshuicalendar.models.Category;
import com.hofs.fengshuicalendar.models.DayStars;

public class FragmentListView extends Fragment {
	 private static final String Tag = "Daystars list Empty Error";
		EditText editText;
	  
		List<DayStars> daystarsList;
		List<Category> suitavoidList;
	    ListView list;
    
    public FragmentListView() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listview, null);
        saveDataToPreferences(this);
        list = (ListView) view.findViewById(R.id.list);
        /* String[] id = new String[daystarsList.size()];
      		int i=0;
      		
      		for (DayStars db: daystarsList) {
      		   id[i++] =Integer.toString(db.getId())+" "+db.getName_en()+" "+db.getName_cn();
      		    
      			
      		}
      		
      		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
      			        android.R.layout.simple_list_item_1, id);
      		list.setAdapter(adapter);
      */
        String[] id = new String[suitavoidList.size()];
  		int i=0;
  		
  		for (Category db: suitavoidList) {
  		   id[i++] =Integer.toString(	db.getId())+"\n"+
  				   						"name_en="+db.getName_en()+"\n"+
  				   						"name_cn"+db.getName_cn()+"\n"+
  				   						"description_en"+db.getDescription_en()+"\n"+
  				   						"description_cn"+db.getDescription_cn()+"\n"+
  				   						"suit_avoid"+db.getSuitAvoid();
  		   								
  		    
  			
  		}
  		
  		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
  			        android.R.layout.simple_list_item_1, id);
  		list.setAdapter(adapter);
        return view;
    }
    public void saveDataToPreferences(Fragment fragment) {
  		// TODO Auto-generated method stub
  		
  			Preferences p=new Preferences(fragment.getActivity());
  			String xml=p.get("result");
  			if(xml==""){
  	    	   Log.d(Tag,"empty result");
  			}
  			else{
  				XmlParser parser=new XmlParser(xml);
  				//this.daystarsList=parser.getDayStarsList();
  				this.suitavoidList=parser.getSuitAvoidList();
  			}
  	}
}
