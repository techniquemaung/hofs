package com.hofs.fengshuicalendar.dashboard.fragments;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import com.hofs.fengshuicalendar.R;
import com.hofs.fengshuicalendar.dashboard.storage.Preferences;
import com.hofs.fengshuicalendar.xmlparser.XmlParser;
import com.hofs.fengshuicalendar.xmlparser.XmlStringReader;

import com.hofs.fengshuicalendar.models.DayBreaker;
import com.hofs.fengshuicalendar.models.DayOfficer;

public class FragmentChangeDateOfBirth extends Fragment {
    private static final String Tag ="DayOfficer List Empty ";
	CheckBox checkBox1;
    CheckBox checkBox2;
    ListView list;
   	String str;
  
   	List<DayOfficer> dayofficerList;
    public FragmentChangeDateOfBirth() {
    	
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_changedateofbirth, null);
        saveDataToPreferences(this);
     //  getDayBreakerList(this);
     /* TextView text = (TextView) view.findViewById(R.id.text);
       text.setText(str);*/
     list = (ListView) view.findViewById(R.id.list);
        String[] id = new String[dayofficerList.size()];
      		int i=0;
      		
      		for (DayOfficer db: dayofficerList) {
      		   id[i++] =Integer.toString(	db.getId())+"\n"+
      				   						"name_en="+db.getName_en()+"\n"+
      				   						"name_cn="+db.getName_cn()+"\n"+
      				   						"status="+db.getStatus();
      		    
      			//id+=Integer.toString(db.getId());
      		}
      		
      		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
      			        android.R.layout.simple_list_item_1, id);
      		list.setAdapter(adapter);
        
        //checkBox1 = (CheckBox) view.findViewById(R.id.fragment_checkbox_checkbox1);
        //checkBox2 = (CheckBox) view.findViewById(R.id.fragment_checkbox_checkbox2);
        
        return view;
    }
    public void saveDataToPreferences(Fragment fragment) {
  		// TODO Auto-generated method stub
  		
  		Preferences p=new Preferences(fragment.getActivity());
  		String xml=p.get("result");
  	       if(xml==""){
  	    	   Log.d(Tag,"empty result");
  	       }
  	       else{
  	      XmlParser parser=new XmlParser(xml);
  	     //this.daystarsList=parser.getDayStarsList();
  	   this.dayofficerList=parser.getDayOfficerList();
  	       }
  	}
  	
}
