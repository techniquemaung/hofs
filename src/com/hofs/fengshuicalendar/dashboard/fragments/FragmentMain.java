package com.hofs.fengshuicalendar.dashboard.fragments;


import java.io.StringReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.hofs.fengshuicalendar.R;
import com.hofs.fengshuicalendar.dashboard.DashBoardActivity;
import com.hofs.fengshuicalendar.dashboard.storage.Preferences;
import com.hofs.fengshuicalendar.xmlparser.XmlParser;

import com.hofs.fengshuicalendar.models.DayBreaker;
import com.hofs.fengshuicalendar.models.DayOfficer;

public class FragmentMain extends Fragment {
	
    public static TextView datetext;
    TextView today,tvxml;
    ImageView circle,hour,dailypersonal,suitable,unsuitable,dayofficer;
    int key=0;
    String string;
    List<DayBreaker> daybreakerList;
    List<DayOfficer> dayofficerList;
    ListView list;

	public  FragmentMain() {
		
    	
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, null);
        /*  textView = (TextView) view.findViewById(R.id.fragment_main_textview);
        circle=(ImageView) view.findViewById(R.id.cycle);
        */
        
        saveDataToPreferences(this);
       /* list = (ListView) view.findViewById(R.id.list);
        String[] id = new String[daybreakerList.size()];
      		int i=0;
      		
      		for (DayBreaker db: daybreakerList) {
      		   id[i++] =Integer.toString(db.getId())+" "+db.getName_en()+" "+db.getName_cn();
      		    
      			//id+=Integer.toString(db.getId());
      		}
      		
      		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
      			        android.R.layout.simple_list_item_1, id);
      		list.setAdapter(adapter);*/
      /*		
       datetext = (TextView) view.findViewById(R.id.datetext);
       datetext.setText(DashBoardActivity.strDate);
       today= (TextView) view.findViewById(R.id.today);
       today.setText("Hofs Xml file");*/
    /*
       list=(ListView)view.findViewById(R.id.daybreaker);
       
       String[] id = new String[dayofficerList.size()];
		int i=0;
		
		for (DayOfficer db: dayofficerList) {
		   id[i++] =Integer.toString(db.getId())+" "+db.getName_en()+" "+db.getName_cn();
		    
			//id+=Integer.toString(db.getId());
		}
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
			        android.R.layout.simple_list_item_1, id);
		list.setAdapter(adapter);
		*/
       
		tvxml= (TextView) view.findViewById(R.id.xml);
	   tvxml.append(string);
       /*
       circle=(ImageView) view.findViewById(R.id.cycle);
       dailypersonal=(ImageView) view.findViewById(R.id.dailypersonal);
       hour=(ImageView) view.findViewById(R.id.show1);
      suitable=(ImageView) view.findViewById(R.id.suitable);
       unsuitable=(ImageView) view.findViewById(R.id.unsuitable);
       dayofficer=(ImageView) view.findViewById(R.id.dayofficer);
      
       final ViewGroup popup = (RelativeLayout) view.findViewById(R.id.sliding1);
       popup.setVisibility(View.GONE);

             
              hour.setOnClickListener(new View.OnClickListener() {

                       @Override
                       public void onClick(View arg0) {
                               if(key==0){
                                       key=1;
                                       popup.setVisibility(View.VISIBLE);
                                       hour.setBackgroundResource(R.drawable.hourstarshead);
                                      //hour.setBackgroundResource(R.drawable.hourstars);
                               }
                               else if(key==1){
                                       key=0;
                                       popup.setVisibility(View.GONE);
                                       hour.setBackgroundResource(R.drawable.hourstarshead);
                                      // hour.setBackgroundResource(R.drawable.hourstars);
                               }
                       }
               });

              final ViewGroup popup2 = (RelativeLayout) view.findViewById(R.id.sliding2);
              popup2.setVisibility(View.GONE);

                     
                     dailypersonal.setOnClickListener(new View.OnClickListener() {

                              @Override
                              public void onClick(View arg0) {
                                      if(key==0){
                                              key=1;
                                              popup2.setVisibility(View.VISIBLE);
                                              dailypersonal.setBackgroundResource(R.drawable.dailypesonalhead);
                                            
                                      }
                                      else if(key==1){
                                              key=0;
                                              popup2.setVisibility(View.GONE);
                                              dailypersonal.setBackgroundResource(R.drawable.dailypesonalhead);
                                            
                                      }
                              }
                      });
                     final ViewGroup popup3 = (RelativeLayout) view.findViewById(R.id.sliding3);
                     popup3.setVisibility(View.GONE);

                            
                            suitable.setOnClickListener(new View.OnClickListener() {

                                     @Override
                                     public void onClick(View arg0) {
                                             if(key==0){
                                                     key=1;
                                                     popup3.setVisibility(View.VISIBLE);
                                                     suitable.setBackgroundResource(R.drawable.suitexpander);
                                                   
                                             }
                                             else if(key==1){
                                                     key=0;
                                                     popup3.setVisibility(View.GONE);
                                                     suitable.setBackgroundResource(R.drawable.suitexpander);
                                                   
                                             }
                                     }
                             });
                            final ViewGroup popup4 = (RelativeLayout) view.findViewById(R.id.sliding4);
                            popup4.setVisibility(View.GONE);

                                   
                                   unsuitable.setOnClickListener(new View.OnClickListener() {

                                            @Override
                                            public void onClick(View arg0) {
                                                    if(key==0){
                                                            key=1;
                                                            popup4.setVisibility(View.VISIBLE);
                                                            unsuitable.setBackgroundResource(R.drawable.unsuitablehead);
                                                          
                                                    }
                                                    else if(key==1){
                                                            key=0;
                                                            popup4.setVisibility(View.GONE);
                                                            unsuitable.setBackgroundResource(R.drawable.unsuitablehead);
                                                          
                                                    }
                                            }
                                    });
        
                                   final ViewGroup popup5 = (RelativeLayout) view.findViewById(R.id.sliding5);
                                   popup5.setVisibility(View.GONE);

                                          
                                          dayofficer.setOnClickListener(new View.OnClickListener() {

                                                   @Override
                                                   public void onClick(View arg0) {
                                                           if(key==0){
                                                                   key=1;
                                                                   popup5.setVisibility(View.VISIBLE);
                                                                   dayofficer.setBackgroundResource(R.drawable.dayofficerhead);
                                                                 
                                                           }
                                                           else if(key==1){
                                                                   key=0;
                                                                   popup5.setVisibility(View.GONE);
                                                                   dayofficer.setBackgroundResource(R.drawable.dayofficerhead);
                                                                  
                                                           }
                                                   }
                                           });
               */
        return view;
    }

	public void setData(String str) {
		// TODO Auto-generated method stub
		this.string=str;
	}

	public void setDayBreakerList(List<DayBreaker> dayBreakerList) {
		// TODO Auto-generated method stub
		this.daybreakerList=dayBreakerList;
		
	}

/*	public void saveDataToPreferences(Context context) {
		// TODO Auto-generated method stub
		
		Preferences p=new Preferences(context);
		// this.string=p.get("result");
	      String xml=p.get("result");
	     XmlParser parser=new XmlParser(xml);
	   this.daybreakerList=parser.getDayBreakerList();
	    // this.dayofficerList=parser.getDayOfficerList();
		
	}*/
	 public void saveDataToPreferences(Fragment fragment) {
			// TODO Auto-generated method stub
			
			Preferences p=new Preferences(fragment.getActivity());
		this.string =p.get("result");
		      // String xml=p.get("result");
		  // XmlParser parser=new XmlParser(xml);
		   //  this.daybreakerList=parser.getDayBreakerList();
		  //  this.dayofficerList=parser.getDayOfficerList();
			
		}
	
}
