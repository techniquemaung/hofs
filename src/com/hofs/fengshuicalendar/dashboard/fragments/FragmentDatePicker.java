package com.hofs.fengshuicalendar.dashboard.fragments;

import java.util.Calendar;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;

import com.hofs.fengshuicalendar.R;

public class FragmentDatePicker extends Fragment {
    DatePicker datepicker;
    
    public FragmentDatePicker() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_datepicker, null);
    
        datepicker=(DatePicker)view.findViewById(R.id.datePicker1);
        final Calendar c = Calendar.getInstance();
   		int year = c.get(Calendar.YEAR);
   		int month = c.get(Calendar.MONTH);
   		int day = c.get(Calendar.DAY_OF_MONTH);

   		
   		
   
   		datepicker.init(year, month, day, null);
        
        return view;
    }
}
