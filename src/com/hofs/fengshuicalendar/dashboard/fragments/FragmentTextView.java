package com.hofs.fengshuicalendar.dashboard.fragments;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.hofs.fengshuicalendar.R;
import com.hofs.fengshuicalendar.dashboard.storage.Preferences;
import com.hofs.fengshuicalendar.xmlparser.XmlParser;

import com.hofs.fengshuicalendar.models.Almanac;
import com.hofs.fengshuicalendar.models.DayStars;

public class FragmentTextView extends Fragment {
	private static final String Tag = "Daystars list Empty Error";
	List<Almanac> almanacList;
    ListView list;
    
    public FragmentTextView() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_textview, null);
    
        saveDataToPreferences(this);
        
    	list = (ListView) view.findViewById(R.id.list);
    	String[] id = new String[almanacList.size()];
  		int i=0;
  		
  		for (Almanac db: almanacList) {
  		   id[i++] =Integer.toString(
  				   db.getId())+"\n"+
  				  "date_en="+ db.getDateEn()+"\n"+
  				  "date_cn="+ db.getDateCn()+"\n"+
  				  "rating="+ db.getRating()+"\n"+
  				  "day_officer="+ db.getDayOfficer()+"\n"+
  				  "dong_gong="+ db.getDongGong()+"\n"+
  				  "yb_belt="+ db.getYBBelt()+"\n"+
  				  "special_info_en="+db.getSpecialInfoEn()+"\n"+
  				  "special_info_cn="+db.getSpecialInfoCn()+"\n"+
  				  "noble_man_direction="+ db.getNobleManDirection()+"\n"+
  				  "happiness_direction="+ db.getHappinessDirection()+"\n"+
  				  "wealth_direction"+ db.getWealthDirection()+"\n"+
  				  "day_breaker="+ db.getDayBreaker()+"\n"+
  				  "suit="+ db.getSuit()+"\n"+
  				  "avoid="+ db.getAvoid()+"\n"+
  				  "day_stars="+ db.getDayStars()+"\n"+
  				  "hour_stars="+ db.getHourStars()
  				   
  				   ;
  		    
  			//id+=Integer.toString(db.getId());
  		}
  		
  		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
  			        android.R.layout.simple_list_item_1, id);
  		list.setAdapter(adapter);
  		
  
        return view;
    }
    public void saveDataToPreferences(Fragment fragment) {
  		// TODO Auto-generated method stub
  		
  			Preferences p=new Preferences(fragment.getActivity());
  			String xml=p.get("result");
  			if(xml==""){
  	    	   Log.d(Tag,"empty result");
  			}
  			else{
  				XmlParser parser=new XmlParser(xml);
  				this.almanacList=parser.getAlmanacList();
  	  
  			}
  	}
}
