package com.hofs.fengshuicalendar.dashboard.fragments;

import java.util.List;

import com.hofs.fengshuicalendar.R;
import com.hofs.fengshuicalendar.dashboard.storage.Preferences;
import com.hofs.fengshuicalendar.xmlparser.XmlParser;

import com.hofs.fengshuicalendar.models.DayStars;
import com.hofs.fengshuicalendar.models.Direction;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

public class FragmentImageView extends Fragment {
    //ImageView imageView;
	 private static final String Tag = "Daystars list Empty Error";
		EditText editText;
	   // List<DayOfficer> dayofficerList;
		//List<DayStars> daystarsList;
		List<Direction> directionList;
	    ListView list;
    public FragmentImageView() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_imageview, null);
       /* 
        imageView = (ImageView) view.findViewById(R.id.fragment_imageview_imageview);
        
        imageView.setImageResource(R.drawable.android);
        */
        saveDataToPreferences(this);
        /* list = (ListView) view.findViewById(R.id.list);
         String[] id = new String[dayofficerList.size()];
       		int i=0;
       		
       		for (DayOfficer db: dayofficerList) {
       		   id[i++] =Integer.toString(db.getId())+" "+db.getName_en()+" "+db.getName_cn();
       		    
       			//id+=Integer.toString(db.getId());
       		}
       		
       		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
       			        android.R.layout.simple_list_item_1, id);
       		list.setAdapter(adapter);
       		*/
       list = (ListView) view.findViewById(R.id.list);
         String[] id = new String[directionList.size()];
       		int i=0;
       		
       		for (Direction db: directionList) {
       		   id[i++] =Integer.toString(	db.getId())+"\n"+
       				   						"name_en="+db.getName_en()+"\n"+
       				   						"name_cn="+db.getName_cn();
       		    
       			//id+=Integer.toString(db.getId());
       		}
       		
       		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
       			        android.R.layout.simple_list_item_1, id);
       		list.setAdapter(adapter);
       		
       //  editText = (EditText) view.findViewById(R.id.fragment_edittext_edittext);
         
        return view;
    }
    public void saveDataToPreferences(Fragment fragment) {
  		// TODO Auto-generated method stub
  		
  		Preferences p=new Preferences(fragment.getActivity());
  		String xml=p.get("result");
  	       if(xml==""){
  	    	   Log.d(Tag,"empty result");
  	       }
  	       else{
  	      XmlParser parser=new XmlParser(xml);
  	    // this.daystarsList=parser.getDayStarsList();
  	  //  this.dayofficerList=parser.getDayOfficerList();
  	      this.directionList=parser.getDirectionList();
  	       }
  	}
}
