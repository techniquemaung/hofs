package com.hofs.fengshuicalendar.dashboard.mainfragments;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;


import com.hofs.fengshuicalendar.R;
import com.hofs.fengshuicalendar.dashboard.DashBoardActivity;
import com.hofs.fengshuicalendar.dashboard.storage.Preferences;
import com.hofs.fengshuicalendar.xmlparser.XmlParser;




import com.hofs.fengshuicalendar.models.Almanac;
import com.hofs.fengshuicalendar.models.Category;
import com.hofs.fengshuicalendar.models.DayBreaker;
import com.hofs.fengshuicalendar.models.DayOfficer;
import com.hofs.fengshuicalendar.models.SuitAvoid;
import com.hofs.fengshuicalendar.models.YBBelt;

public class FragmentMain2 extends Fragment {
	
	private static final String Tag = "Daystars list Empty Error";
	List<Almanac> almanacList;
	List<DayOfficer> dayOfficerList;
	List<DayBreaker> dayBreakerList;
	List<YBBelt> ybBeltList;
	List<Category> suitAvoidList;
    ListView list;
	
   
    public TextView tv ;
    TextView period1data,period2data,period3data,period4data,period5data,period6data;
    TextView period7data,period8data,period9data,period10data,period11data,period12data;
    ImageView donggongstar1,donggongstar2,donggongstar3,donggongstar4,donggongstar5;
    TextView dayname,daybreakername,dayofficername,ybbeltname,specialinfotitle,specialinfo,suitableactivitiestitle,suitableactivities,unsuitableactivities,unsuitableactivitiestitle;
    String dateStr;String strtext,dateforshow;
   
    boolean running;
	ProgressWheel pw_two;
	
	int progress = 0;
	
	Date date,currentDate,startDate, endDate;
    Bitmap bitmap;
    String zodiac_sign;
	
	
	public  FragmentMain2() {
		
    	
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	View view = inflater.inflate(R.layout.mainfragment_main, null);
    	
    	   
    	
    	
      	ReadAndParseDataFromPreferences(this);
      	
       	tv=(TextView) view.findViewById(R.id.textView2);
       	daybreakername=(TextView) view.findViewById(R.id.dayBreakerName);
       	dayofficername=(TextView) view.findViewById(R.id.dayOfficerName);
       	ybbeltname=(TextView) view.findViewById(R.id.ybbeltname);
       	suitableactivities=(TextView) view.findViewById(R.id.suitableActivities);
    	unsuitableactivities=(TextView) view.findViewById(R.id.unsuitableActivities);
       	suitableactivitiestitle=(TextView) view.findViewById(R.id.suitableActivitiesText);
       	unsuitableactivitiestitle=(TextView) view.findViewById(R.id.unsuitableActivitiesText);
    	specialinfotitle=(TextView) view.findViewById(R.id.specialInfoTitle);
       	specialinfo=(TextView) view.findViewById(R.id.specialInfoText);
       	
    	donggongstar1=(ImageView)view.findViewById(R.id.star1);
    	donggongstar2=(ImageView)view.findViewById(R.id.star2);
    	donggongstar3=(ImageView)view.findViewById(R.id.star3);
    	donggongstar4=(ImageView)view.findViewById(R.id.star4);
    	donggongstar5=(ImageView)view.findViewById(R.id.star5);
       
        ImageView image = new ImageView(getActivity());
        image.setImageResource(R.drawable.starforavg);
        image.setLayoutParams(new FrameLayout.LayoutParams(120, 30, Gravity.CENTER));
       


       	
    	period1data=(TextView) view.findViewById(R.id.period1data);
    	period2data=(TextView) view.findViewById(R.id.period2data);
    	period3data=(TextView) view.findViewById(R.id.period3data);
    	period4data=(TextView) view.findViewById(R.id.period4data);
    	period5data=(TextView) view.findViewById(R.id.period5data);
    	period6data=(TextView) view.findViewById(R.id.period6data);
    	
    	period7data=(TextView) view.findViewById(R.id.period7data);
    	period8data=(TextView) view.findViewById(R.id.period8data);
    	period9data=(TextView) view.findViewById(R.id.period9data);
    	period10data=(TextView) view.findViewById(R.id.period10data);
    	period11data=(TextView) view.findViewById(R.id.period11data);
    	period12data=(TextView) view.findViewById(R.id.period12data);
    	
       	pw_two = (ProgressWheel) view.findViewById(R.id.progressBarTwo);
       
       	
        
       		 
       	strtext=getArguments().getString("DateForSign");
       	dateforshow=getArguments().getString("DateForShow");
       	tv.setText(dateforshow);
       	dateStr=getArguments().getString("date");
       		
       
       	for (Almanac db: almanacList) {
				    if(db.getDateEn().equalsIgnoreCase(dateStr)){
				        		
				        		period1data.setText(db.getHourStars().get_hs_11pm_1259am());
				        		period2data.setText(db.getHourStars().get_hs_01am_0259am());
				        		period3data.setText(db.getHourStars().get_hs_03am_0459am());
				        		period4data.setText(db.getHourStars().get_hs_05am_0659am());
				        		period5data.setText(db.getHourStars().get_hs_07am_0859am());
				        		period6data.setText(db.getHourStars().get_hs_09am_1059am());
				        		
				        		period7data.setText(db.getHourStars().get_hs_11am_1259pm());
				        		period8data.setText(db.getHourStars().get_hs_01pm_0259pm());
				        		period9data.setText(db.getHourStars().get_hs_03pm_0459pm());
				        		period10data.setText(db.getHourStars().get_hs_05pm_0659pm());
				        		period11data.setText(db.getHourStars().get_hs_07pm_0859pm());
				        		period12data.setText(db.getHourStars().get_hs_09pm_1059pm());
				        	
			     
										        	
				        		int dayofficerid=db.getDayOfficer();
								for(DayOfficer dbo: dayOfficerList ){
												        			
										if(dbo.getId()==dayofficerid){
												        dayofficername.setText(dbo.getName_en().trim());
												       
										}
								}
										        	
									          	
								int daybreakerid=db.getDayBreaker();
								for(DayBreaker dbb: dayBreakerList ){
										if(dbb.getId()==daybreakerid){
														daybreakername.setText(dbb.getName_en().trim())	;					
										 }
										        				
										
								}
								
								int ybbeltid=db.getYBBelt();
								for(YBBelt dby: ybBeltList ){
										if(dby.getId()==ybbeltid){
														ybbeltname.setText(dby.getName_en().trim())	;					
										 }
										        				
										
								}
									       
								int donggong=db.getDongGong();
								if (donggong==1){
													        				donggongstar1.setBackgroundResource(R.drawable.yellowstar);
													        				donggongstar2.setBackgroundResource(R.drawable.averagestar);
													        				donggongstar3.setBackgroundResource(R.drawable.averagestar);
													        				donggongstar4.setBackgroundResource(R.drawable.averagestar);
													        				donggongstar5.setBackgroundResource(R.drawable.averagestar);
													        				
								}
								if (donggong==2){
													        				donggongstar1.setBackgroundResource(R.drawable.yellowstar);
													        				donggongstar2.setBackgroundResource(R.drawable.yellowstar);
													        				donggongstar3.setBackgroundResource(R.drawable.averagestar);
													        				donggongstar4.setBackgroundResource(R.drawable.averagestar);
													        				donggongstar5.setBackgroundResource(R.drawable.averagestar);
								}
								if (donggong==3){
													        				donggongstar1.setBackgroundResource(R.drawable.yellowstar);
													        				donggongstar2.setBackgroundResource(R.drawable.yellowstar);
													        				donggongstar3.setBackgroundResource(R.drawable.yellowstar);
													        				donggongstar4.setBackgroundResource(R.drawable.averagestar);
													        				donggongstar5.setBackgroundResource(R.drawable.averagestar);
													        				
								}
								if (donggong==4){
													        				donggongstar1.setBackgroundResource(R.drawable.yellowstar);
													        				donggongstar2.setBackgroundResource(R.drawable.yellowstar);
													        				donggongstar3.setBackgroundResource(R.drawable.yellowstar);
													        				donggongstar4.setBackgroundResource(R.drawable.yellowstar);
													        				donggongstar5.setBackgroundResource(R.drawable.averagestar);
													        				
								}
								if (donggong==5){
													        				donggongstar1.setBackgroundResource(R.drawable.yellowstar);
													        				donggongstar2.setBackgroundResource(R.drawable.yellowstar);
													        				donggongstar3.setBackgroundResource(R.drawable.yellowstar);
													        				donggongstar4.setBackgroundResource(R.drawable.yellowstar);
													        				donggongstar5.setBackgroundResource(R.drawable.yellowstar);
													        				
								}
									      
												
								String specialInfo_en=db.getSpecialInfoEn();
								if (specialInfo_en!=null){
											
										specialinfotitle.setVisibility(View.VISIBLE);
										specialinfo.setText(specialInfo_en.trim());
								}
								
		 
								String suitStr=db.getSuit();
								if(suitStr!=null){
											
									    	String[] suit=this.explode(suitStr);
									    	
									    	int[] integer=new int[suit.length];
									 	for (int t=0;t<suit.length;t++){
									    		
									 		integer[t]=Integer.parseInt(suit[t]);
									    	}
									  
									    	
									   	if(suit!=null){
												    				suitableactivitiestitle.setVisibility(View.VISIBLE);
												    	
												    				
												    			
												    				for(int index=0;index<suit.length;index++){
													        					for(Category dbs: suitAvoidList){
													        						List<SuitAvoid> saList=dbs.getSuitAvoid();
													        						for(SuitAvoid dbsv: saList){
													        							
														        						if(integer[index]==dbsv.getId()){
														        								
														        							suitableactivities.append("\n"+Integer.toString(index+1)+"."+dbsv.getName_en().trim());
													        							}
													        					}
															        						
													        					}
												    				}
												    				
									    	}	
								}
								String unsuitStr=db.getAvoid();
								if(unsuitStr!=null){
											
									    	String[] unsuit=this.explode(unsuitStr);
									    	
									    	int[] integer=new int[unsuit.length];
									 	for (int t=0;t<unsuit.length;t++){
									    		
									 		integer[t]=Integer.parseInt(unsuit[t]);
									    	}
									   
									    	
									   	if(unsuit!=null){
												    				unsuitableactivitiestitle.setVisibility(View.VISIBLE);
												    	
												    				
												    			
												    				for(int index=0;index<unsuit.length;index++){
													        					for(Category dbs: suitAvoidList){
													        						List<SuitAvoid> saList=dbs.getSuitAvoid();
													        						for(SuitAvoid dbsv: saList){
													        							
														        						if(integer[index]==dbsv.getId()){
														        								
														        							unsuitableactivities.append("\n"+Integer.toString(index+1)+"."+dbsv.getName_en().trim());
													        							}
													        					}
															        						
													        					}
												    				}
												    				
									    	}	
								}	
												
				  }								
		   	
       }
	  		
       	
 
       	
       	final Runnable r = new Runnable() {
			public void run() {
				running = true;
				while(progress<361) {
					pw_two.incrementProgress();
					progress++;
					try {
						  for (Almanac db: almanacList) {
							        	if(db.getDateEn().equalsIgnoreCase(dateStr)){
							        		
											        		if(db.getRating().equalsIgnoreCase("G")){
											        			pw_two.setBarColor(Color.GREEN);
											        			pw_two.setTextColor(Color.YELLOW);
											        			
											        			pw_two.setTextSize(18);
											        			
											        			pw_two.setText("\nToday is\n\nGOOD DAY");
											        		}
											        		if(db.getRating().equalsIgnoreCase("A")){
											        			pw_two.setBarColor(Color.BLUE);
											        			pw_two.setTextColor(Color.YELLOW);
											        			pw_two.setTextSize(18);
											        			
											        			pw_two.setText("\nToday is\n\nAVERAGE DAY");
											        		}
											        		if(db.getRating().equalsIgnoreCase("B")){
											        			pw_two.setBarColor(Color.RED);
											        			pw_two.setTextColor(Color.YELLOW);
											        			pw_two.setTextSize(18);
											        			
											        			pw_two.setText("\nToday is\n\nBAD DAY");
											        		}
							        		
							        	
							        	}
					        }
							
							Thread.sleep(15);
					} catch (InterruptedException e) {
							
							e.printStackTrace();
					}
				}
				running = false;
			}
      };
      if(!running) {
			progress = 0;
			pw_two.resetCount();
			Thread s = new Thread(r);
			s.start();
      }
       
   
	
      return view;
    }

    @Override
	public void onPause() {
		super.onPause();
		progress = 361;
		pw_two.stopSpinning();
		pw_two.resetCount();
		pw_two.setText("Click\none of the\nbuttons");
	}
    public void ReadAndParseDataFromPreferences(Fragment fragment) {
  		// TODO Auto-generated method stub
  		
  			Preferences p=new Preferences(fragment.getActivity());
  			String xml=p.get("result");
  			if(xml==""){
  	    	   Log.d(Tag,"empty result");
  			}
  			else{
  				XmlParser parser=new XmlParser(xml);
  				this.almanacList=parser.getAlmanacList();
  				this.dayOfficerList=parser.getDayOfficerList();
  				this.dayBreakerList=parser.getDayBreakerList();
  				this.suitAvoidList=parser.getSuitAvoidList();
  				this.ybBeltList=parser.getYBBeltList();
  	  
  			}
  	}
  

    public  String[] explode(String s) {
    	String str=s.substring(1, s.length() - 1);
    	//s.replaceAll("[^\\w\\s]" ,"");
    	 String[] values=  str.split(",");
       
        return values;
    }
   
}
   