package com.hofs.fengshuicalendar.dashboard.mainfragments;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;

import com.hofs.fengshuicalendar.R;
import com.hofs.fengshuicalendar.dashboard.storage.Preferences;
import com.hofs.fengshuicalendar.xmlparser.XmlParser;

import com.hofs.fengshuicalendar.models.DayStars;
import com.hofs.fengshuicalendar.models.YBBelt;

public class FragmentRadioButton extends Fragment {
	 private static final String Tag = "YBbelt list Empty Error";
		List<YBBelt> ybbeltList;
	    ListView list;
    
    public FragmentRadioButton() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mainfragment_radiobutton, null);
        saveDataToPreferences(this);
        
    	list = (ListView) view.findViewById(R.id.list);
    	String[] id = new String[ybbeltList.size()];
  		int i=0;
  		
  		for (YBBelt db: ybbeltList) {
  		   id[i++] =Integer.toString(	db.getId())+"\n"+
  				   						"name_en="+db.getName_en()+"\n"+
  				   						"name_cn="+db.getName_cn()+"\n"+
  				   						"status="+db.getStatus();
  		    
  			//id+=Integer.toString(db.getId());
  		}
  		
  		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
  			        android.R.layout.simple_list_item_1, id);
  		list.setAdapter(adapter);
  		
       
        return view;
    }
    public void saveDataToPreferences(Fragment fragment) {
  		// TODO Auto-generated method stub
  		
  			Preferences p=new Preferences(fragment.getActivity());
  			String xml=p.get("result");
  			if(xml==""){
  	    	   Log.d(Tag,"empty result");
  			}
  			else{
  				XmlParser parser=new XmlParser(xml);
  				this.ybbeltList=parser.getYBBeltList();
  	  
  			}
  	}
}
