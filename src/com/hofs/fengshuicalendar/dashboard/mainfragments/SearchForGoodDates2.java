package com.hofs.fengshuicalendar.dashboard.mainfragments;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.hofs.fengshuicalendar.R;
import com.hofs.fengshuicalendar.dashboard.MainDashBoardActivity;
import com.hofs.fengshuicalendar.dashboard.storage.Preferences;
import com.hofs.fengshuicalendar.xmlparser.XmlParser;

import com.hofs.fengshuicalendar.models.Almanac;
import com.hofs.fengshuicalendar.models.Category;
import com.hofs.fengshuicalendar.models.DayBreaker;
import com.hofs.fengshuicalendar.models.DayOfficer;
import com.hofs.fengshuicalendar.models.SuitAvoid;
import com.hofs.fengshuicalendar.models.YBBelt;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class SearchForGoodDates2 extends Fragment {
	private static final String Tag = "Daystars list Empty Error";
	 	ListView list;
	 	
		List<Category> suitAvoidList;
	 	String categ_name_en;
	 	private String[] lvMenuItems;
		   
	    public SearchForGoodDates2() {
	    	 
	    }

	    @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {
	    	
	       View view = inflater.inflate(R.layout.mainfragment_search, null);
	       
	       ReadAndParseDataFromPreferences(this);
	       
	       categ_name_en=getArguments().getString("categoryName_en");
	       list= (ListView) view.findViewById(R.id.list);
	       int i=0;
	     // System.out.println("cat_Name_en"+categ_name_en);
	      
	     
		   for (Category db: suitAvoidList) {
			   
			   if(db.getName_en().trim().equalsIgnoreCase(categ_name_en)){
				   for (SuitAvoid dbs: db.getSuitAvoid()) {
					   //lvMenuItems[i]= dbs.getName_en();
				   		i++;
				   }
				   
				   lvMenuItems=new String[i];
				  
				   i=0;
				   for (SuitAvoid dbs: db.getSuitAvoid()) {
					   lvMenuItems[i]= dbs.getName_en().trim();
					   i++;
				   }
			   }
			   
		   }
		   for(int j=0;j<lvMenuItems.length;j++)
		  
		   list.setAdapter(new ArrayAdapter<String>(this.getActivity(),android.R.layout.simple_list_item_1,lvMenuItems));
	       list.setOnItemClickListener(new OnItemClickListener() {
	           @Override
	           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	               onMenuItemClick(parent, view, position, id);
	           }
	           
	       });
	     
	   
		
	        return view;
	    }
	    private void onMenuItemClick(AdapterView<?> parent, View view, int position, long id) {
	    	
	        String selectedItem = lvMenuItems[position];
	        FragmentManager fm = this.getFragmentManager();
	        FragmentTransaction ft = fm.beginTransaction();
	        Fragment fragment = null;
	       
	        	 fragment = new DatesSearched();
	        	 Bundle bundle=new Bundle();
	        	 bundle.putString("categoryName_en",selectedItem);
				 /* bundle.putString("DateForSign", dateForSign.toString());
				 bundle.putString("date", new SimpleDateFormat("yyyy-MM-dd").format(c.getTime()));*/
				 fragment.setArguments(bundle);
	        	
	     
	         if(fragment != null) {
	             
	             ft.replace(R.id.placeforfragment, fragment);
	             ft.commit();
	             
	            
	             MainDashBoardActivity.tvTitle.setText(selectedItem);
	         }
	         
	      
	        
	        
	     
	         
	         
	        
	    }
	    public void ReadAndParseDataFromPreferences(Fragment fragment) {
	  		// TODO Auto-generated method stub
	  		
	  			Preferences p=new Preferences(fragment.getActivity());
	  			String xml=p.get("result");
	  			if(xml==""){
	  	    	   Log.d(Tag,"empty result");
	  			}
	  			else{
	  				XmlParser parser=new XmlParser(xml);
	  				
	  				this.suitAvoidList=parser.getSuitAvoidList();
	  				
	  	  
	  			}
	  	}
	}
