package com.hofs.fengshuicalendar.dashboard.mainfragments;






import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.hofs.fengshuicalendar.R;
import com.hofs.fengshuicalendar.dashboard.MainDashBoardActivity;
import com.hofs.fengshuicalendar.dashboard.layout.MainDashBoardLayout;

public class MenuListFragment extends Fragment {
   ListView list;
   private String[] lvMenuItems;
   MainDashBoardLayout mLayout ;  
    public MenuListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	
        View view = inflater.inflate(R.layout.mainfragment_search, null);
       
       list= (ListView) view.findViewById(R.id.list);
       lvMenuItems = getResources().getStringArray(R.array.mainmenuitems);
       int i=lvMenuItems.length;
       this.expand();
      
       
       lvMenuItems[i]=MainDashBoardActivity.str;
       
       
       list.setAdapter(new ArrayAdapter<String>(this.getActivity(),android.R.layout.simple_list_item_1, lvMenuItems));
       list.setOnItemClickListener(new OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               onMenuItemClick(parent, view, position, id);
           }
           
       });
      
      
        return view;
    }
   private void onMenuItemClick(AdapterView<?> parent, View view, int position, long id) {
	   
	   String selectedItem = lvMenuItems[position];
       String currentItem = MainDashBoardActivity.tvTitle.getText().toString();
       
     
       if(selectedItem.compareTo(currentItem) == 0) {
           this.mLayout.toggleMenu2();
           return;
       }
        
        FragmentManager fm = this.getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = null;
        
        if(selectedItem.compareTo("Search for Good Dates") == 0) {
        	 fragment = new SearchForGoodDates();
        } else if(selectedItem.compareTo("Change Date Of Birth") == 0) {
        	fragment = new SearchForGoodDates();
        }
        else if(selectedItem.compareTo("Language") == 0) {
        	fragment = new SearchForGoodDates();
        } else if(selectedItem.compareTo("In-App Purchase") == 0) {
        	fragment = new SearchForGoodDates();
        } else if(selectedItem.compareTo("Consultation Form") == 0) {
        	fragment = new SearchForGoodDates();
        } else if(selectedItem.compareTo("Feedback Form") == 0) {
        	fragment = new SearchForGoodDates();
        } else if(selectedItem.compareTo("Glossary") == 0) {
        	fragment = new SearchForGoodDates();
        } else if(selectedItem.compareTo("Introduction to App") == 0) {
        	fragment = new SearchForGoodDates();
        }  else if(selectedItem.compareTo(lvMenuItems[lvMenuItems.length-1]) == 0) {
        	fragment = new SearchForGoodDates();
        } 
        
      if(fragment != null) {
             ft.replace(R.id.placeforfragment, fragment);
             ft.commit();
             MainDashBoardActivity.tvTitle.setText(selectedItem);
         }
         
         this.mLayout.toggleMenu2();
      
    }


public void setMLayout(MainDashBoardLayout mainLayout) {
	// TODO Auto-generated method stub
	this.mLayout=mainLayout;
}
public void expand() {
    String[] newArray = new String[lvMenuItems.length + 1];
    System.arraycopy(lvMenuItems, 0, newArray, 0,lvMenuItems.length);
    lvMenuItems = newArray;
}

}