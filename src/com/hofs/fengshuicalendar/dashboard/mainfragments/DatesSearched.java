package com.hofs.fengshuicalendar.dashboard.mainfragments;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.hofs.fengshuicalendar.R;
import com.hofs.fengshuicalendar.dashboard.MainDashBoardActivity;
import com.hofs.fengshuicalendar.dashboard.storage.Preferences;
import com.hofs.fengshuicalendar.xmlparser.XmlParser;
import com.hofs.fengshuicalendar.models.Almanac;
import com.hofs.fengshuicalendar.models.Category;
import com.hofs.fengshuicalendar.models.DayBreaker;
import com.hofs.fengshuicalendar.models.DayOfficer;
import com.hofs.fengshuicalendar.models.SuitAvoid;
import com.hofs.fengshuicalendar.models.YBBelt;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class DatesSearched extends Fragment {
	private static final String Tag = "Daystars list Empty Error";
	List<Almanac> almanacList;
	
	List<Category> suitAvoidList;
	ListView list;
	public String[] lvMenuItems;
	String categ_name_en;
	int id;   
    public DatesSearched() {
    	
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.mainfragment_search, null);
       categ_name_en=getArguments().getString("categoryName_en");
       list= (ListView) view.findViewById(R.id.list);
      /* lvMenuItems = getResources().getStringArray(R.array.search_menu_items);*/
       ReadAndParseDataFromPreferences(this);
       
       
       int i=0;
       for (Category db: suitAvoidList) {
		   
		  
			   for (SuitAvoid dbs: db.getSuitAvoid()) {
				  if(dbs.getName_en().trim().equalsIgnoreCase(categ_name_en)){
					  id=dbs.getId();
				  }
			   }
       }
      
      // System.out.println("category_name_en:"+ categ_name_en);
      // System.out.println("id:"+ id);
       ArrayList alist = new ArrayList();
       
	   	for (Almanac dba: almanacList) {
      
	   		String suitStr=dba.getSuit();
			if(suitStr!=null){
				
				String[] str=this.explode(suitStr);
				String dateSearched="";
				
				
				for(int j=0;j<str.length;j++){
					if(str[j].equalsIgnoreCase(Integer.toString(id))){
						i++;
						dateSearched=dba.getDateEn();
						alist.add( dateSearched );
					}
				}
				
			}
	   	}
	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	    Date date = null;  
	   	lvMenuItems=new String[alist.size()];
	   	for( int j = 0; j < lvMenuItems.length; j++ )
	   	{	try {
			date=sdf.parse(alist.get( j ).toString().trim());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	   	
	   	lvMenuItems[ j ] =  (new SimpleDateFormat("EEE , dd MMM yyyy").format(date)).toString();
	   	}
    	
	   /*	for(int k=0;k<lvMenuItems.length;k++)
	   		System.out.println(lvMenuItems[k]);*/
	   	
	   	
	 
	  // 	System.out.println("no of dates:"+Integer.toString(i));
	   
	   
	  
				   
		   list.setAdapter(new ArrayAdapter<String>(this.getActivity(),android.R.layout.simple_list_item_1,lvMenuItems));
		   list.setOnItemClickListener(new OnItemClickListener() {
	           @Override
	           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	               onMenuItemClick(parent, view, position, id);
	           }
	           
	       });
       
      
   
	
        return view;
    }
private void onMenuItemClick(AdapterView<?> parent, View view, int position, long id) {
    	
        String selectedItem = lvMenuItems[position];
        
      
        FragmentManager fm = this.getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment fragment = null;
        
        
        	
        	 fragment = new FragmentMain2();
            
             Bundle bundle=new Bundle();
     		 bundle.putString("DateForShow",selectedItem);
             bundle.putString("DateForSign", selectedItem);
     		 bundle.putString("date", selectedItem);  
     		 // bundle.putString("date", "kyawsanoo");
     		 fragment.setArguments(bundle);
        
         
         
        
    
        if(fragment != null) {
            
            ft.replace(R.id.placeforfragment, fragment);
            ft.commit();
            
           
            MainDashBoardActivity.tvTitle.setText(selectedItem);
        }
        
         
        
    }
    public void ReadAndParseDataFromPreferences(Fragment fragment) {
  		// TODO Auto-generated method stub
  		
  			Preferences p=new Preferences(fragment.getActivity());
  			String xml=p.get("result");
  			if(xml==""){
  	    	   Log.d(Tag,"empty result");
  			}
  			else{
  				XmlParser parser=new XmlParser(xml);
  				this.almanacList=parser.getAlmanacList();
  				
  				this.suitAvoidList=parser.getSuitAvoidList();
  				
  	  
  			}
  	}
    public  String[] explode(String s) {
    	String str=s.substring(1, s.length() - 1);
    	//s.replaceAll("[^\\w\\s]" ,"");
    	 String[] values=  str.split(",");
       
        return values;
    }
}	
        	
          
          
           
            
    
