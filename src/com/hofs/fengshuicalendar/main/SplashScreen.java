package com.hofs.fengshuicalendar.main;


import com.hofs.fengshuicalendar.R;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

public class SplashScreen extends Activity {

	private static final int SPLASH_DISPLAY_TIME = 2000;  /* 2 seconds */

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// hide titlebar of application
        // must be before setting the layout
      /*  requestWindowFeature(Window.FEATURE_NO_TITLE);*/
        // hide statusbar of Android
        // could also be done later
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.splash);
		
		 /* Create a new handler with which to start the main activity
		    and close this splash activity after SPLASH_DISPLAY_TIME has
		    elapsed. */
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				
				/* Create an intent that will start the main activity. */
				Intent mainIntent = new Intent(SplashScreen.this,
					LanguagesActivity.class);
				SplashScreen.this.startActivity(mainIntent);
				
				/* Finish splash activity so user cant go back to it. */
				SplashScreen.this.finish();
				
				/* Apply our splash exit (fade out) and main
				   entry (fade in) animation transitions. */
				overridePendingTransition(R.anim.languageoptions_fadein,
					R.anim.splash_fadeout);
			}
		}, SPLASH_DISPLAY_TIME);
	}
}