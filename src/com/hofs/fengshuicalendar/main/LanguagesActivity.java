package com.hofs.fengshuicalendar.main;




import com.hofs.fengshuicalendar.R;


import android.util.Log;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Color;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class LanguagesActivity extends ListActivity {
	 
   // List<XmlValuesModel> myData = null;
     	TextView tv1,tv2;
     	String[] listItems={"English", "China"}; 
 		boolean[] listImages={true,  true};
 		ImageView ib;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
 
        super.onCreate(savedInstanceState);
       
        requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        setContentView(R.layout.languageoption);
       // getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,R.layout.customizetitle);
        tv1=(TextView)findViewById(R.id.textView2);
        ib=(ImageView)findViewById(R.id.imageView1);
		        
		    	ib.setOnClickListener(new OnClickListener(){
					public void onClick(View v){
					Intent intent = new Intent(getApplicationContext(),SplashScreen.class);
						
						startActivity(intent);
						
				}
				});
        
        
        
        tv1.setOnClickListener(new OnClickListener(){
			public void onClick(View v){
			Intent intent = new Intent(getApplicationContext(), SplashScreen.class);
				
				startActivity(intent);
				
		}
		});
        setListAdapter(new ImageAdapter(this, R.layout.languageoption, R.id.text1, R.id.right_arrow, listItems, listImages ));
    }
    
    @Override 
    public void onListItemClick(ListView l, View v, int position, long id) {
        // Do something when a list item is clicked
		  // get selected items
		
	    String selectedValue = (String) getListAdapter().getItem(position);
	            if(selectedValue.equalsIgnoreCase("English"))
	    {
	        Intent in = new Intent(getApplicationContext(),com.hofs.fengshuicalendar.login.eng.WelcomeActivity.class);
	        startActivity(in);
	    }
	    
	       

    }
    

}
        