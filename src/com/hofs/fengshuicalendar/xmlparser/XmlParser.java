package com.hofs.fengshuicalendar.xmlparser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.xml.sax.InputSource;

import android.app.Activity;
import android.os.Bundle;
import android.os.StrictMode;

import android.text.Layout.Directions;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.hofs.fengshuicalendar.R;
import com.hofs.fengshuicalendar.dashboard.storage.Preferences;

import com.hofs.fengshuicalendar.models.Almanac;
import com.hofs.fengshuicalendar.models.Category;
import com.hofs.fengshuicalendar.models.DayBreaker;
import com.hofs.fengshuicalendar.models.DayOfficer;
import com.hofs.fengshuicalendar.models.DayStars;
import com.hofs.fengshuicalendar.models.Direction;
import com.hofs.fengshuicalendar.models.Result;
import com.hofs.fengshuicalendar.models.SuitAvoid;
import com.hofs.fengshuicalendar.models.YBBelt;


public class XmlParser {
	String xml="";
	
	Result r=new Result();
	public XmlParser(String xmlStr){
		this.xml=xmlStr;
	}
	public  List<DayBreaker> getDayBreakerList(){
		String str="";
		Serializer serializer = new Persister();        
		Reader reader = new StringReader(this.xml);
		
		try {
			this.r= serializer.read(Result.class, reader, false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return r.getData().getDayBreakerList();
	}
	public  List<DayOfficer> getDayOfficerList(){
		String str="";
		Serializer serializer = new Persister();        
		Reader reader = new StringReader(this.xml);
		
		try {
			this.r= serializer.read(Result.class, reader, false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return r.getData().getDayOfficerList();
	}
	public  List<DayStars> getDayStarsList(){
		String str="";
		Serializer serializer = new Persister();        
		Reader reader = new StringReader(this.xml);
		
		try {
			this.r= serializer.read(Result.class, reader, false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return r.getData().getDayStarsList();
	}
	public  List<Direction> getDirectionList(){
		String str="";
		Serializer serializer = new Persister();        
		Reader reader = new StringReader(this.xml);
		
		try {
			this.r= serializer.read(Result.class, reader, false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return r.getData().getDirectionList();
	}
	public  List<Category> getSuitAvoidList(){
		String str="";
		Serializer serializer = new Persister();        
		Reader reader = new StringReader(this.xml);
		
		try {
			this.r= serializer.read(Result.class, reader, false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return r.getData().getSuitAvoidList().getProperties();
	}
	public  List<YBBelt> getYBBeltList(){
		String str="";
		Serializer serializer = new Persister();        
		Reader reader = new StringReader(this.xml);
		
		try {
			this.r= serializer.read(Result.class, reader, false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return r.getData().getYBBeltList();
	}
	public  List<Almanac> getAlmanacList(){
		String str="";
		Serializer serializer = new Persister();        
		Reader reader = new StringReader(this.xml);
		
		try {
			this.r= serializer.read(Result.class, reader, false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return r.getData().getAlmanacList();
	}
/*
	public String getCode(){
		return Integer.toString(r.getCode());
	}
	public String getDescription(){
		return r.getDescription();
	}
	public String getDownloadLocation(){
		return r.getDownload().getLocation();
	}
	public String getExpDate(){
		return r.getData().getDate();
	}
	public List<DayBreaker> getDayBreakerList(){
		return r.getData().getDayBreakerList();
	}
	public List<DayOfficer> getDayOfficerList(){
		return r.getData().getDayOfficerList();
	}
	public List<DayStars> getDayStarsList(){
		return r.getData().getDayStarsList();
	}
	public List<Direction> getDirectionList(){
		return r.getData().getDirectionList();
	}
	public List<Category> getSuitAvoidList(){
		return r.getData().getSuitAvoidList().getProperties();
	}
	public List<YBBelt> getYBBeltList(){
		return r.getData().getYBBeltList();
	}
	 public List<Almanac> getAlmanacList() {
	      return r.getData().getAlmanacList();
	 } 
	
*/
}