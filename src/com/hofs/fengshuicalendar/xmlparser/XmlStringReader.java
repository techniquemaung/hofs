package com.hofs.fengshuicalendar.xmlparser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import android.content.Context;

public class XmlStringReader {
	 //InputStream is = getResources().openRawResource(R.raw.sample);
	Context con;
	public XmlStringReader(Context c){
		this.con=c;
	}
	public String getXmlString(){
		
	
	    InputStream is = null;
	     try {
				is = con.getAssets().open("newdata.xml");
			} catch (IOException e1) {
				
				e1.printStackTrace();
			}
	     Writer writer = new StringWriter();
	     char[] buffer = new char[1024];
	     try {
	         Reader reader=null;
				try {
					reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				} catch (UnsupportedEncodingException e) {
					
					e.printStackTrace();
				}
	         int n;
	         try {
					while ((n = reader.read(buffer)) != -1) {
					    writer.write(buffer, 0, n);
					}
				} catch (IOException e) {
					
					e.printStackTrace();
				}
	     } finally {
	         try {
					is.close();
				} catch (IOException e) {
					
					e.printStackTrace();
				}
	     }
	
	     
	    final String xmlData = writer.toString();
	     return xmlData;
	}
}
